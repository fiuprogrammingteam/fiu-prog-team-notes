#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

//----------------------------------------------------------------------//
typedef pair<double,double> dd;
int N;
vector<dd> points;

double dist(dd p1,dd p2){
	return hypot(p2.first - p1.first, p2.second - p1.second);
}

bool xSorter(dd a , dd b){
	return a.first < b.first;
}
bool ySorter(dd a , dd b){
	return a.second < b.second;
}

double closestPoints2(const vector<dd> &P,const vector<dd> &Q){
	if(P.size() < 2)return 1000000.00;
	if(P.size() == 2)return dist(P[0], P[1]);

	int mid = P.size() / 2;

	vector<dd> Pl, Pr, Ql, Qr;

	for (int i = 0; i < P.size(); ++i) {
		if(i < mid)
			Pl.push_back(P[i]);
		else
			Pr.push_back(P[i]);
	}

	for (int i = 0; i < Q.size(); ++i) {
		if(Q[i].first < P[mid].first - 1e-9)
			Ql.push_back(Q[i]);
		else
			Qr.push_back(Q[i]);
	}

	double rec1 = closestPoints2(Pl,Ql);
	double rec2 = closestPoints2(Pr,Qr);

	double s = min(rec1, rec2);

	vector<dd> strip;

	for (int i = 0; i < Q.size(); ++i)
		if(fabs(Q[i].first - P[mid].first) <= s)
			strip.push_back(Q[i]);

	for (int i = 0; i < strip.size(); ++i)
		for (int j = i+1; j < strip.size(); ++j) {
			if(strip[j].second - strip[i].second > s + 1e-9)break;
			double tmpDist = dist(strip[j] , strip[i]);
			s = min(s, tmpDist);
		}

	return s;
}

inline void solve(){
	vector<dd> P,Q;
	// TODO write test cases
	printf("Test not implemente!\n");
//	while(scanf("%d", &N) == 1 && N){
//		P.clear(),Q.clear();
//		for (int i = 0; i < N; ++i) {
//			double x,y;scanf("%lf %lf", &x, &y);
//			P.push_back(dd(x,y));
//			Q.push_back(dd(x,y));
//		}
//
//		// Keep two list of points
//		// One sorted by x-coordinates and one sorted by y-coordinates
//		sort(P.begin(), P.end(), xSorter);
//		sort(Q.begin(), Q.end(), ySorter);
//		double ans = closestPoints2(P,Q);
//
//		if(ans < 10000.0 - 1e-9)
//			printf("%.4lf\n",ans);
//		else
//			printf("INFINITY\n");
//	}

}
int main(){
		solve();
	return 0;
}
