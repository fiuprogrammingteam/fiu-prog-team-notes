#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

#define max_n 3000000
int ft[max_n];
int n;
int rsq(int i){
	int s = 0;
	for (; i >0; i -= i&(-i)) s += ft[i];
	return s;
}

void update(int i, int val){
	for (; i <= n; i += i&(-i)) ft[i] += val;
}

//----------------------------------------------------------------------//
#define CAT_NUMBERS 30
ll pascalTriangle[10000][10000];
ll catalanNumbers[CAT_NUMBERS];
ll superCatalanNumbers[CAT_NUMBERS];

class Permutations{
	int n;
	int level;
	vi permu;

	Permutations(int _n){
		n = _n;
		level = -1;
		permu.assign(n,-1);
		go();
	}

	void go(){

		if(level >= n-1){
			for (int i = 0; i < n; ++i) {
				printf("%d ", permu[i]);
			}
			printf("\n");
			return;
		}
		level++;
		for(int j = 0 ; j < n ; j++){
			if(permu[j] == -1){
				permu[j] = level;
				go();
				permu[j] = -1;
			}
		}
		level--;
	}
};

ll factorial(int n, int k);

/**
 * Use formula directly
 */
ll binomialCoef(int n, int k);

/**
 *  Combinations with repetition. C(n +k -1 , k )
 */
ll combinationsRep(int n, int k);

/**
 * Use top-down dp
 * create pascal triangle
 */
ll bcDp(int n, int k);

/**
 * Creates catalan table
 */
void buildCatalanNumbers();

/**
 * Get the super catalan number n
 */
ll getSuperCatalanNumbers(int n);

/**
 * Get catalan numeber n
 */
ll Cat(int n);

/**
 * gets the of the set {1,2,3....,n}
 */
vi getNthPermutation(int size, int n);

/**
 * get the factorial number for this permutation
 */
vi factoradic(const  vi &per);

/**
 * return the permutation for this factoradic number
 */
vi getPermutation(const vi & f);

/**
 * Derangement
 * d(n) = (n-1)*(d(n-1)+d(n-2));
 */
ll derangement(ll n);

int main(){
	MEM(superCatalanNumbers, -1);
//	printf("cat(%d) = %lld\n", 4, getSuperCatalanNumbers(4));
	vi p = getNthPermutation(3,2);

//	for (int i = 0; i < p.size(); ++i) {
//		printf("%d ", p[i]);
//	}
//	printf("\n");

	// Derangement
	assert(derangement(2) == 1);
	assert(derangement(3) == 2);
	assert(derangement(4) == 9);
	assert(derangement(5) == 44);

	printf("All test passed\n");
	return 0;
}

ll factorial(int n, int k){
	ll product = 1;
	for (ll i = n; i > k; --i)
		product *= i;
	return product;
}

ll binomialCoef(int n, int k){
	if(k < (n - k)){
		k = n - k;
	}
	ll num = factorial(n,k);
	ll den = factorial(n - k,0);
	return num/den;
}

ll combinationsRep(int n, int k){
	return binomialCoef(n + k - 1, k);
}

ll bcDp(int n, int k){
	ll &ret = pascalTriangle[n][k];
	if(ret != -1 )return ret;
	if(k == 0 || n == k )return ret = 1;
	return ret = bcDp(n-1,k-1) + bcDp(n-1,k);
}

void buildCatalanNumbers(){
	catalanNumbers[0] = 1;
	for (int n = 1; n < CAT_NUMBERS; ++n)
		catalanNumbers[n] = (((2*n)*(2*n-1))*catalanNumbers[n-1])/((n+1)*n);
}

ll getSuperCatalanNumbers(int n){
	ll &ret = superCatalanNumbers[n];
	if(n <= 2)return 1;
	if(ret == -1)
		ret = (3*(2*n - 3)*getSuperCatalanNumbers(n-1) - (n-3)*getSuperCatalanNumbers(n-2))/n;
	return ret;
}

ll Cat(int n){
	return catalanNumbers[n];
}


vi getNthPermutation(int size, int n){
	vi p,t,factoraic;
	int i = 1;

	for (int i = 0; i < size; ++i)
		t.push_back(i);

	while(i <= size){
		factoraic.push_back(n % i);
		n /= i;
		i++;
	}
	while(!factoraic.empty()){
		int pos = factoraic.back();factoraic.pop_back();
		p.push_back(t[pos]);
		vi::iterator it = t.begin();
		advance(it, pos);
		t.erase(it);
	}
	return p;
}

vi factoradic(const  vi &per){
	MEM(ft,0);
	vi f;

	for (int i = 0; i < n; ++i) {
		int a = per[i];
		a -= rsq(per[i]+1);
		update(per[i]+1,1);
		f.push_back(a);
	}

	return f;
}

vi getPermutation(const vi & f){
	MEM(ft,0);
	vi per;
	for (int i = 1; i <= n; ++i)
		update(i,1);
	for (int i = 0; i < n; ++i) {
		int l = 1 , r = n,p,m;

		while(l<=r){
			m = (l + r) >> 1;
			if(rsq(m)-1<f[i]){
				l = m+1;
				p = m+1;
			}else
				r = m -1;
		}

		update(l,-1);
		per.push_back(l-1);
	}

	return per;
}

ll derangement(ll n){
	ll D[n+1];
	D[0] = D[1] = 0;
	D[2] = 1;
	for (int i = 3; i <= n; ++i)
		D[i] = (i-1)*(D[i-1] + D[i-2]);
	return D[n];
}


