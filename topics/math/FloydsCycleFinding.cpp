#include <bits/stdc++.h>
using namespace std;

const int  MAX_INT = std::numeric_limits<int>::max();
const int  MIN_INT = std::numeric_limits<int>::min();
const int INF = 1000000000;
const int  NEG_INF = -1000000000;

#define max(a,b)(a>b?a:b)
#define min(a,b)(a<b?a:b)
#define FOR(i,s,n)for(i=s;i<n;i++)
#define REP(n)for(int i = 0 ; i < n ; i++)
#define MEM(arr,val)memset(arr,val, sizeof arr)
#define PI acos(0)*2.0
#define eps 1.0e-9
#define iszero(a)fabs(a)<eps
#define LS(b)(b& (-b)) // Least significant bit

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int gcd(int a,int b){return b == 0 ? a : gcd(b,a%b);}
int lcm(int a,int b){return a*(b/gcd(a,b));}

//----------------------------------------------------------------------//

ll f(ll x){
	return (3*x + 5) % 12;
}
/**
 * mu  : the first element of the sequence
 * lambda : the length of of the sequence
 */
ii floydCycleFinding(ll x0){
	// 1st: finding k*lambda
	ll tortoise = f(x0), hare = f(f(x0));
	while(tortoise != hare){
		tortoise = f(tortoise), hare = f(f(hare));
	}
	// 2nd: finding mu
	int mu = 0;
	hare = x0;
	while(tortoise != hare){
		tortoise = f(tortoise), hare = f(hare);
		mu++;
	}
	int lambda = 1;
	hare = f(tortoise);
	while(tortoise != hare){
		hare = f(hare);
		lambda++;
	}
	return ii(mu, lambda);
}

int main(){

	ii c1 = floydCycleFinding(7);
//	printf("for seed %d  the cycle is: mu = %d lambda = %d\n",7, c1.first, c1.second);
	assert(c1.first == 1 && c1.second == 2);
	printf("All test passed\n");
	return 0;
}
