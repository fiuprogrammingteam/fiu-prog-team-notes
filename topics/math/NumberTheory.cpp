//
#include <bits/stdc++.h>
using namespace std;

const int  MAX_INT = std::numeric_limits<int>::max();
const int  MIN_INT = std::numeric_limits<int>::min();
const int INF = 1000000000;
const int  NEG_INF = -1000000000;

#define max(a,b)(a>b?a:b)
#define min(a,b)(a<b?a:b)
#define FOR(i,s,n)for(i=s;i<n;i++)
#define REP(n)for(int i = 0 ; i < n ; i++)
#define MEM(arr,val)memset(arr,val, sizeof arr)
#define PI acos(0)*2.0
#define eps 1.0e-9
#define iszero(a)fabs(a)<eps
#define abs_val(a)((a)>=0)?(a):(-(a))

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}


//----------------------------------------------------------------------//
#define MAX_PRIMES 1000000
vi primes;
bitset<MAX_PRIMES> is_prime;

/**
 * Sieves of eratosthenes
 */
void sieves();

/**
 * Prime factorization
 */
void factorize(ll n, vector<ii> &f);

ll mod_factorial(ll n, ll r, ll m);

ll modPow(ll b, ll n, ll m);

/* This function calculates (a^b)%c */
ll modulo(ll a,ll b,ll c);

/**
 * The number of prime factors
 */
ll numPF(ll N);

/**
 * The number of different prime factors
 */
ll numDiffPF(ll N);

/**
 * The sum of the prime factors
 */
ll sumPF(ll N);

/**
 * The number of divisors of N
 */
ll numDiv(ll N);

/**
 * The sum of the divisors of N
 */
ll sumDiv(ll N);

/**
 * Count the number of positive integers < N that are relative primes to N
 */
ll eulerPhi(ll N);

ll mulmod(ll a, ll b, ll c); // (a * b) % c

/*
 * is p prime
 */
bool Miller(ll p,int iteration);

/**
 * returns a factor of n
 */
ll pollard_rho(ll n);

#define MAX_MAT_SZ 100
class AugMatrix{
public:
	double mat[MAX_MAT_SZ][MAX_MAT_SZ];
};
class ColumnVector{
public:
	double vec[MAX_MAT_SZ];
};

ColumnVector GaussianElimination(int N , AugMatrix Aug);

int main(){
	sieves();
	vii factors;
	int n = 60;
	factorize(n, factors);

//	printf("%d = ", n);
//	for (int i = 0; i < factors.size(); ++i){
//		if(i)printf(" + ");
//			printf("%d^%d ", factors[i].first,factors[i].second);
//	}
//	printf("\n");

	assert(factors[0].first == 2 && factors[0].second == 2);
	assert(factors[1].first == 3 && factors[1].second == 1);
	assert(factors[2].first == 5 && factors[2].second == 1);

	assert(modPow(2,3,10) == 8);
	assert(modPow(2,4,10) == 6);

	AugMatrix test;
	test.mat[0][0] = 2;
	test.mat[0][1] = 3;
	test.mat[0][2] = 1;
	test.mat[1][0] = 6;
	test.mat[1][1] = 3;
	test.mat[1][2] = 2;

	ColumnVector v = GaussianElimination(2, test);
//	for (int i = 0; i < 2; ++i) {
//		printf("%.2lf\n",v.vec[i]);
//	}
	assert(fabs(v.vec[0] - 0.25) < 1e-2);
	assert(fabs(v.vec[1] - 0.17) < 1e-2);
	int cnt = 0;

	for (int i = 2; i < 10000; ++i) {
		cnt += eulerPhi(i);
	}
	assert(cnt==30393485);

	printf("All test passed\n");
	return 0;
}


ColumnVector GaussianElimination(int N , AugMatrix Aug){
	int i, j, k,l;ColumnVector X;	double t;
	for(j = 0 ; j < N -1; j++){
		l = j;
		for(i = j +1  ; i < N ; i++)
			if(fabs(Aug.mat[i][j]) > fabs(Aug.mat[l][j]))
				l = i;
		for(k = j ; k <= N ;k++)
			t = Aug.mat[j][k], Aug.mat[j][k] = Aug.mat[l][k], Aug.mat[l][k] = t;

		for(i = j +1 ; i < N ; i++){
			for(k = N ; k >= j ;k--){
				Aug.mat[i][k] -= Aug.mat[j][k] * Aug.mat[i][j] / Aug.mat[j][j];
			}
		}
	}

	for(j = N -1; j >= 0 ;j--){
		for(k = j + 1, t = 0.0 ; k < N ; k++)
			t += Aug.mat[j][k] * X.vec[k];
		X.vec[j] = (Aug.mat[j][N] - t )/ Aug.mat[j][j];

	}
	return X;
}

void factorize(ll n, vector<ii> &f){
	ll i = n;
	int j = 0;
	while(primes[j]*primes[j] <= i){
		ii p_factor(primes[j], 0);
		while(i % primes[j] == 0){
			p_factor.second++;
			i /= primes[j];
		}
		if(p_factor.second){
			f.push_back(p_factor);
		}
		j++;
	}
	if(i > 1){
		f.push_back(ii(i,1));
	}
}


ll mod_factorial(ll n, ll r, ll m){

	ll res = 1;
	int c = 0;
	while(n > r){
		res = (res*n);
		while(res && res % 10 == 0)res /= 10;
		res = res % m;
		n--;
	}
	return res;
}

ll modPow(ll b, ll n, ll mod){
	ll res = 0;
	if(n <= 0)return 1;
	if(n&1){
		res = modPow(b,n/2,mod);
		res = (res*res*b) % mod;
	}else{
		res = modPow(b,n/2,mod);
		res = (res*res) % mod;
	}
	return res;
}

ll mulmod(ll a, ll b, ll c){ // (a * b) % c
	ll x = 0, y = a % c;
	while(b > 0){
		if(b % 2 == 1) x = (x + y) % c;
		y = (y * 2) %c;
		b /= 2;
	}
	return x % c;
}

/* This function calculates (a^b)%c */
ll modulo(ll a,ll b,ll c){
    long long x=1,y=a; // long long is taken to avoid overflow of intermediate results
    while(b > 0){
        if(b%2 == 1){
//            x=(x*y)%c;
            x=mulmod(x,y,c);
        }
        y = mulmod(y,y,c); // squaring the base
        b /= 2;
    }
    return x%c;
}

ll numPF(ll N){
	ll PF_idx = 0, PF = primes[PF_idx], ans = 0;

	while(PF*PF <= N){
		while(N % PF == 0){N /= PF; ans++;}
		PF = primes[++PF_idx];
	}

	if(N != 1 )ans++;
	return ans;
}

ll numDiffPF(ll N){
	ll PF_idx = 0, PF = primes[PF_idx], ans = 0;

	while(PF*PF <= N){
		if(N % PF == 0)
			ans++;
		while(N % PF == 0){N /= PF;}
		PF = primes[++PF_idx];
	}

	if(N != 1 )ans++;
	return ans;
}

ll sumPF(ll N){
	ll PF_idx = 0, PF = primes[PF_idx], ans = 0;

	while(PF*PF <= N){
		while(N % PF == 0){N /= PF;ans += PF;}
		PF = primes[++PF_idx];
	}

	if(N != 1 )ans += N;;
	return ans;
}


ll numDiv(ll N){
	ll PF_idx = 0, PF = primes[PF_idx], ans = 1;

	while(PF*PF <= N){
		int c = 0;
		while(N % PF == 0){N /= PF;c++;}
		ans *= (c + 1);
		PF = primes[++PF_idx];
	}

	if(N != 1 )ans *=  2;

	return ans;
}


ll sumDiv(ll N){
	ll PF_idx = 0, PF = primes[PF_idx], ans = 1;

	while(PF*PF <= N){
		ll power = 0;
		while(N % PF == 0){N /= PF;power++;}

		ans *= ((ll)pow(PF, power + 1.0) - 1) / (PF - 1);
		PF = primes[++PF_idx];
	}

	if(N != 1 )ans *= ((ll)pow(N, 2.0) - 1) / (N - 1);

	return ans;
}


ll eulerPhi(ll N){
	ll PF_idx = 0, PF = primes[PF_idx], ans = N;

	while(PF*PF <= N){
		if(N % PF == 0)ans -= ans / PF;
		while(N % PF == 0){N /= PF;}

		PF = primes[++PF_idx];
	}
	if(N != 1 )ans -= ans / N;
	return ans;
}


void sieves(){
	is_prime.set();
	for (int i = 4; i < MAX_PRIMES; i+=2)
		is_prime[i] = false;

	for (int i = 3; i*i < MAX_PRIMES; i += 2)
		if(is_prime[i])
			for (int j = i*i; j < MAX_PRIMES; j += i)
				is_prime[j] = false;

	primes.push_back(2);
	for (int i = 3; i < MAX_PRIMES; i += 2) {
		if(is_prime[i])
			primes.push_back(i);
	}
}


bool Miller(ll p,int iteration){
    if(p<2){
        return false;
    }
    if(p!=2 && p%2==0){
        return false;
    }
    ll s=p-1;
    while(s%2==0){
        s/=2;
    }
    for(int i=0;i<iteration;i++){
        ll a=rand()%(p-1)+1,temp=s;
        ll mod=modulo(a,temp,p);
        while(temp!=p-1 && mod!=1 && mod!=p-1){
            mod = mulmod(mod,mod,p);
            temp *= 2;
        }

        if(mod!=p-1 && temp%2==0){
        	// mod == 1
            return false;
        }
    }
    return true;
}


ll pollard_rho(ll n){
	ll i = 0,j = 0, k = 2;
	ll x, y ,d, seed = 3;
	d = n;
	for(ll c = 3;d == n || d == 1;c++){

		x = (mulmod(seed,seed,n) + c) % n;
		y = seed;
		i = 1;
		k = 1;
		while(x != y){
			if(i == k){
				y = x;
				k *= 2;
				i = 0;
			}
			x = (mulmod(x,x,n) + c) % n;int
			d = gcd(abs_val(y - x), n);
			if(d > 1)break;
			i++;
		}
	}
	return d;
}
