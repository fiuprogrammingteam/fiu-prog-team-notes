#include <cstdio>
#include <string>
#include <vector>
#include <algorithm>
#include <assert.h>

using namespace std;

string naive_lrs(vector<string> strs){
    string longest;
    for(int i = 0 ; i < (int)strs[0].size() ; i++){
        for(int j = i + 1 ; j < (int)strs[0].size() ; j++){
            
            int substringSize = j - i;
            string substrig = strs[0].substr(i , substringSize); 
            
            int  reapeats = 0;
            for(int k = 1; k < strs.size() ; k++){
                for(int z = 0; z < (int)(strs[k].size() - substringSize + 1) ; z++){
                    string substrig2 = strs[k].substr(z , substringSize);
                    if(substrig == substrig2 ){
                        reapeats++;
                        break;
                    }
                }
            }
            
            if(reapeats == (int)(strs.size() - 1)){
                if(substrig.size() > longest.size())longest = substrig;
            }
        }
    }
    return longest;
}

int lcs(string a, string b){
    int i;
    
    for(i = 0 ; i < a.size() || b.size() ; i++)
        if(a[i] != b[i])break;
        
    return i ;
}

string suffixtree_sol(vector<string> strs){
    vector<string > suffix_tree;
    
    for(int i = 0 ; i < strs.size() ; i++)
        for(int j = 0 ; j < strs[i].size() ; j++){
            string suffix = strs[i].substr(j, (int)( strs[i].size()) );
            suffix_tree.push_back(suffix);
        }
        
    sort(suffix_tree.begin() , suffix_tree.end());
    string longest = "";
    for(int i = 0 ; i< (int)(suffix_tree.size() - 1); i++){
     
        int len = lcs(suffix_tree[i], suffix_tree[i+1]);
        if(len > (int)longest.size())longest = suffix_tree[i].substr(0, len);
    }
    
    return longest;
}

int main(){
     
    vector<string> strs;
    strs.push_back("abcrepeatzzalala");
    strs.push_back("secondrepeataaaaa");
    strs.push_back("moretestrepeat");
    strs.push_back("repeathola!!!!");
    
    // Find the longest repeating substring
    // string lrs = naive_lrs(strs);
    string lrs = suffixtree_sol(strs);
//    printf("lrs =%s\n", lrs.c_str());
    assert(lrs == "repeat");
    
    return 0;
}
