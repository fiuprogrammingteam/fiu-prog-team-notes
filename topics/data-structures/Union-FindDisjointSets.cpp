#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

/**
 * __builtin_popcount(int d) // count bits
 * strtol(s, &end, base); // convert base number
 */
//----------------------------------------------------------------------//
int n;
vi P,H;

void init(int sz){
	n=sz;
	P.assign(sz,-1);
	H.assign(sz,1);
}
int find(int u){
	if(P[u] < 0)return u;
	return P[u] = find(P[u]);
}
bool same(int u, int v){
	return find(u) == find(v);
}
void join(int u, int v){
	if(same(u,v))return;
	u = find(u);
	v= find(v);
	if(P[u]<P[v]){
		P[v] =u;
		H[u]+=H[v];
	}else if(P[v]<P[u]){
		P[u]=v;
		H[v]+=H[u];
	}else{
		P[v]=u;
		H[u]+=H[v];
		P[u]--;
	}
}

int main(){
	FASTER;
	init(100);

	assert(same(0,1) == 0);
	join(0,1);
	assert(same(0,1) == 1);
	assert(same(0,2) == 0);
	join(2,1);
	assert(same(0,2) == 1);
	assert(P[find(1)] == -2);
	assert(P[find(3)] == -1);
	join(3,4);
	assert(P[find(3)] == -2);
	join(1,4);
	assert(P[find(3)] == -3);
	assert(same(0,2) == 1);

	printf("All test passed!\n");
	return 0;
}
