#include <bits/stdc++.h>
using namespace std;

const int  INF = std::numeric_limits<int>::max()/3;

#define max(a,b)(a>b?a:b)
#define min(a,b)(a<b?a:b)
#define MEM(arr,val)memset(arr,val, sizeof arr)
#define PI (acos(0)*2.0)
#define eps 1.0e-9
#define are_equal(a,b)fabs(a-b)<eps
#define LS(b)(b&(-b)) // Least significant bit

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

// ---------------------------------------------------------------------------------------

class point{
public:
	int x,y,z;
	point(int _x, int _y,int _z) : x(_x),y(_y),z(_z){}
	point() {x=y=z=0;}
	void debug();
	double dist(point q);
	bool same(point q);
};
void point::debug(){printf("(%d %d %d)\n", x,y,z);}
double point::dist(point q){
	double dx = x - q.x,dy = x - q.x,dz = x - q.x;
	return sqrt(dx*dx + dy*dy+dz*dz);
}
bool point::same(point q){
	return x == q.x && y == q.y && x == q.z;
}

class KDNode{
public :
	point p;
	KDNode *l,*r;
	int level;
	KDNode(point _p, int lev);
	int diff(point pt);
	ll countRange(point pt,ll D);
	ll distsq(point pt);
	point findNearest(point pt);
	void findNearest(point pt, point &q, double &Dist);
};
KDNode::KDNode(point _p, int lev) : p(_p), level(lev){
	l = r = NULL;
}

int KDNode::diff(point pt){
	switch(this->level){
	case 0:return pt.x - this->p.x;
	case 1:return pt.y - this->p.y;
	case 2:return pt.z - this->p.z;
	}
	return 0;
}
ll KDNode::distsq(point pt){
	ll 	delX = this->p.x - pt.x,
		delY = this->p.y - pt.y,
		delZ = this->p.z - pt.z;
	return delX*delX + delY*delY + delZ*delZ;
}

ll KDNode::countRange(point pt, ll D){

	int ans = (this->distsq(pt) < D*D) ;

	int d = this->diff(pt);

	if(d <= D && l != NULL)
		ans += l->countRange(pt,D);

	if(-d <= D && r != NULL)
		ans += r->countRange(pt,D);

	return ans;
}

point KDNode::findNearest(point pt){
	point q;
	double Dist = 1e18;
	KDNode::findNearest(pt, q,Dist);
	return q;
}

void KDNode::findNearest(point pt, point &q, double &Dist){

	double tdist = sqrt(this->distsq(pt));

	if(!pt.same(this->p) && tdist < Dist){
		Dist = tdist;
		q = this->p;
	}

	double d = this->diff(pt);

	if(d <= Dist && l != NULL)
		l->findNearest(pt,q,Dist);

	if(-d <= Dist && r != NULL)
		r->findNearest(pt,q,Dist);
}


KDNode* insert(KDNode *r, point p, int parent){

	if(r == NULL){
		r = new KDNode(p,(parent + 1) % 3);
	}else{
		int d = r->diff(p);
		if(d <= 0) 	r->l = insert(r->l, p, r->level);
		else 		r->r = insert(r->r, p, r->level);
	}
	return r;
}

KDNode *root;
void data();
void test1();

int main(){
	data();
	test1();

}
void data(){
	root = NULL;
	root = insert(root, point(0,0,0), 0);
	root = insert(root, point(0,1,0), 0);
	root = insert(root, point(10,0,0), 0);
	root = insert(root, point(5,-1,1), 0);
	root = insert(root, point(6,-2,1), 0);
	root = insert(root, point(6,-3,0), 0);
	root = insert(root, point(10,1,1), 0);
	root = insert(root, point(10,-1,18), 0);
	root = insert(root, point(14,-1,10), 0);
	root = insert(root, point(16,-3,0), 0);
	root = insert(root, point(16,1,0), 0);
}
void test1(){
	point p;

	p = root->findNearest(point(10,2,1));
	assert(p.x == 10 && p.y == 1 && p.z == 1);
	p = root->findNearest(point(0,2,0));
	assert(p.x == 0 && p.y == 1 && p.z == 0);

	printf("All test passed\n");
}
