#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

class FenwickTree{
    public :
    vector<int> ft;
    int LSOne(int i){
        return i&(-i);
    }
    FenwickTree(int size){
        ft.assign(size + 1,0);
    }

    int rsq(int i){
        int sum = 0;
        for(; i > 0 ;i-=LSOne(i))
            sum += ft[i];
        return sum;
    }

    void update(int i, int v){
         for(; i < (int)ft.size() ; i += LSOne(i))
            ft[i] += v;
    }
    /**
     * how many values between i and j
     */
    int rsq(int i, int j){
        return rsq(j) - ((i == 1) ? 0 : rsq(i  - 1));
    }
};

void debug(const vi &a){
	for (int i = 0; i < a.size(); ++i) {
		printf("%d ", a[i]);
	}
	printf("\n");
}
/**
 * Count inversions
 */
void compress(vi &a){
	set<int> uniq;
	for (int i = 0; i < a.size(); ++i)uniq.insert(a[i]);
	map<int,int> m;
	for(set<int>::iterator it = uniq.begin() ; it != uniq.end() ; ++it)m[*it] = m.size();
	for (int i = 0; i < a.size(); ++i)a[i] = m[a[i]];
}

int countInv(){
	int n = 6;
	int arr[] = {100,5,14,7,50,25};
	vi test(arr, arr + n);
	compress(test);

	FenwickTree invCount(test.size());
	int total_inversions = 0;
	for (int i = n-1 ; i >= 0; --i) {
		total_inversions += invCount.rsq(test[i]-1);
		invCount.update(test[i],1);
	}
//	printf("total_inversions = %d\n", total_inversions);
	return total_inversions;
}

int main(){
    FenwickTree ft(10);
    int f[] = {2,2,5,5,6,7,8,8,8,8,9};
    
    for(int i = 0 ; i < (int)(sizeof(f)/sizeof(int)) ; i++){
        ft.update(f[i],1);
    }
    
    assert(ft.rsq(2) == 2);
    assert(ft.rsq(5) == 4);
    assert(ft.rsq(6) == 5);
    assert(ft.rsq(8) == 10);
    assert(ft.rsq(5,6) == 3);
    assert(ft.rsq(1,1) == 0);
    assert(ft.rsq(1,2) == 2);
    assert(ft.rsq(1,6) == 5);
    
    assert(countInv() == 7);


    printf("All test passed\n");
    return 0;
}
