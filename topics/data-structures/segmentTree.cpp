#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}
int n;

/**
 * Store the index of the minimum element
 */
class SegmentTree{
    
    public :
		vi st,A;
		SegmentTree(vi &_A){
		  A = _A;
		  n = A.size();
		  st.assign(4*n,0);
		  build();
		}

        
        void build(int p=1, int L=0, int R=n-1){
            if(L == R)
                st[p] = L;
            else{
                build(p*2,  L, (L+R)/2);
                build(p*2+1,  (L+R)/2 +1, R);
                int p1 = st[p*2];
                int p2 = st[p*2+1];
                st[p] = (A[p1] <= A[p2]) ? p1 : p2;
            }
        }
        
        int rmq(int i, int j,int p=1, int L=0, int R=n-1){
            
            if(j < L || i>R)return -1;
            if(L >=i && R <=j)return st[p];
            
            int p1 = rmq(i, j,p*2, L, (L+R)/2);
            int p2 = rmq(i, j,p*2+1, (L+R)/2 + 1, R);
            if(p1 == -1)return p2;
            if(p2 == -1)return p1;
            
            return A[p1] <= A[p2] ? p1 : p2;
        }
        
        void update(int i, int val, int p=1, int L=0, int R=n-1){
            if(L == i && R == i){
                A[i] = val;   
                return;
            }

            int mid = (L + R)/2;
            
            if(i <= mid)
                update(i,val,p*2, L, mid);
            else 
                update(i,val,p*2+1, mid + 1, R);
                
            int p1 = st[p*2];
            int p2 = st[p*2+1];
            
            st[p] = (A[p1] <= A[p2]) ? p1 : p2;
        }
};

/**
 * Maximum range sum
 * Stores the sum of the elements in the range
 */
class SegmentTreeMRS{
	public :
        vi st,A;
		SegmentTreeMRS(vi &_A){
		   A = _A;
		   n = A.size();
		   st.assign(4*n,0);
		   build();
		}
        
        void build(int p=1, int L=0, int R=n-1){
            if(L == R){
                st[p] = A[L];
            }else{
                int mid = (L + R)/2;
                build(p*2,L, mid );
                build(p*2+1, mid + 1, R );
                
                st[p] = st[p*2] + st[p*2+1];
            }
        }
        
        int rsq(int i, int j, int p=1, int L=0, int R=n-1){
            
            if(j < L || i>R)return 0;
            if(L >=i && R <=j)return st[p];
            // if(L == R )return -1;
            
            int p1 = rsq(i,j,p*2, L, (L+R)/2);
            int p2 = rsq(i,j,p*2+1, (L+R)/2 + 1, R);

            return p1+p2;
        }
        void update(int i, int val, int p = 1, int L = 0 , int R = n-1){

        	if(L  == i && R == i){
        		st[p] = A[i] = val;
        		return;
        	}

        	int mid = (L+R)/2;

        	if(i <= mid)
        		update(i,val ,p*2, L, mid);
        	else
        		update(i,val, p*2+1, mid+1,R);

        	st[p] = st[p*2] + st[p*2 + 1];

        }
};


// Range Minimum Query Lazy
class SegmentTreeRangeMinLazy{

    public :
        vi st,A;
        vi lazy;
        vi upt;
        SegmentTreeRangeMinLazy(vi &_A){
            A = _A;
            n = A.size();
            st.assign(4*n,0);
			lazy.assign(4*n,0);
            upt.assign(4*n,0);
            build();
        }
		void build(int p=1, int L = 0, int R =n-1){
			if(L == R){
				st[p] = A[L];
			}else{
				int mid = (L + R)/2;
				build(p*2,L, mid);
				build(p*2+1, mid + 1, R);
				st[p] = min(st[p*2], st[p*2+1]);
			}
		}

        int rmq(int i, int j, int p=1, int L = 0, int R = n-1){

			if(upt[p]){
				st[p] += lazy[p];
				if(R != L){
					lazy[p*2] = lazy[p];
					lazy[p*2+1] = lazy[p];
					upt[p*2] = upt[p*2+1] = 1;
				}
				lazy[p] = -1;
				upt[p] = 0;
			}

            if(j < L || i>R)return 1e9;
            if(L >=i && R <=j)return st[p];

            int p1 = rmq(i,j,	p*2	, L, (L+R)/2);
            int p2 = rmq(i,j,p*2+1	, (L+R)/2 + 1, R);

            return min(p1,p2);
        }

        void update(int i, int j, int val, int p=1, int L=0, int R=n-1){

         	if(upt[p]){
				st[p] += lazy[p];
				if(R != L){
					lazy[(p)] = lazy[p];
					lazy[(p)] = lazy[p];
					upt[p*2] = upt[p*2+1] = 1;
				}
				lazy[p] = 0;
				upt[p] = 0;
			}

         	if(j<L || i>R)return;

            if(L >=i && R <=j){
            	lazy[p] = val;
            	st[p] += lazy[p];
            	if(R != L){
					lazy[p*2] = lazy[p];
					lazy[p*2+1] = lazy[p];
					upt[p*2] = upt[p*2+1] = 1;
				}
				lazy[p] = 0;
				upt[p] = 0;
				return ;
            }

            update(i, j,val,p*2,  L,  (L+R)/2);
            update(i, j,val,p*2+1, (L+R)/2+1, R);

            st[p] = min(st[p*2] , st[p*2+1]);
        }
};

// Maximum range sum Lazy
class SegmentTreeMRSLazy{

    public :
        vi st,A;
        vi lazy;

        SegmentTreeMRSLazy(vi &_A){
			A = _A;
			n = A.size();
			st.assign(4*n,0);
			lazy.assign(4*n,-1);
			build();
		}

        void build(int p=1, int L=0, int R=n-1){
            if(L == R){
                st[p] = A[L];
            }else{
                int mid = (L + R)/2;
                build(p*2,	L, mid );
                build(p*2+1, mid + 1, R);
                st[p] = st[p*2] + st[p*2+1];
            }
        }

        int rsq(int i, int j, int p=1, int L=0, int R=n-1){

         	if(lazy[p] != -1){
				st[p] = lazy[p]*(R - L + 1);
				if(R != L){
					lazy[p*2] = lazy[p];
					lazy[p*2+1] = lazy[p];
				}
				lazy[p] = -1;
			}

            if(j < L || i>R)return 0;
            if(L >=i && R <=j)return st[p];

            int p1 = rsq(i, j, p*2, L, (L+R)/2);
            int p2 = rsq(i, j, p*2+1, (L+R)/2 + 1, R);

            return p1+p2;
        }

        void update(int i, int j, int val, int p=1, int L=0, int R=n-1){

         	if(lazy[p] != -1){
				st[p] = lazy[p]*(R - L + 1);
				if(R != L){
					lazy[p*2] = lazy[p];
					lazy[p*2+1] = lazy[p];
				}
				lazy[p] = -1;
			}

         	if(j < L || i>R)return;

            if(L >=i && R <=j){
            	lazy[p] = val;
            	st[p] = lazy[p]*(R - L + 1);
            	if(R != L){
					lazy[p*2] = lazy[p];
					lazy[p*2+1] = lazy[p];
				}
				lazy[p] = -1;
				return ;
            }

            update(i, j,val, p*2, L, (L+R)/2);
            update(i, j,val, p*2+1, (L+R)/2 + 1, R);
            st[p] = st[p*2] + st[p*2+1];
        }
};


int main(){
    int arr[] = {18,17,13,19,15,11,20};
    vi vec(arr, arr + 7);
    SegmentTree segTree(vec);
    
    assert(segTree.rmq(1,3) == 2);
    assert(segTree.rmq(4,6) == 5);
    segTree.update(5, 30);
    assert(segTree.rmq(4,6) == 4);
    segTree.update(6, 1);
    assert(segTree.rmq(4,6) == 6);
    
    int arr2[] = {1,2,3,4,5};
    vi v2(arr2, arr2 + 5);
    
    SegmentTreeMRS segTreeRsq(v2);
    
    assert(segTreeRsq.rsq(0,5) == 15);
    assert(segTreeRsq.rsq(1,2) == 5);
    assert(segTreeRsq.rsq(1,3) == 9);
    segTreeRsq.update(1,7);
    assert(segTreeRsq.rsq(1,2) == 10);

    SegmentTreeMRSLazy lazySt(v2);

    assert(lazySt.rsq(0,5) == 15);
    lazySt.update(2,4,5);
    assert(lazySt.rsq(0,5) == 18);
    lazySt.update(1,1,10);
    assert(lazySt.rsq(0,1) == 11);
    lazySt.update(0,1,0);
    assert(lazySt.rsq(0,2) == 5);
    assert(lazySt.rsq(0,4) == 15);
    assert(lazySt.rsq(4,4) == 5);


    int arr3[] = {3,2,8,4,5,1,7,8,3,2};
	vi v3(arr3, arr3 + 10);
    SegmentTreeRangeMinLazy lazyRmqSt(v3);

    assert(lazyRmqSt.rmq(0,9) == 1);
    lazyRmqSt.update(4,6,2); // 3 2 8 4 7 3 9 8 3 2
    assert(lazyRmqSt.rmq(0,9) == 2);
    lazyRmqSt.update(0,1,1);  // 4 3 8 4 7 3 9 8 3 2
    assert(lazyRmqSt.rmq(0,1) == 3);
    assert(lazyRmqSt.rmq(0,0) == 4);
    assert(lazyRmqSt.rmq(0,2) == 3);
    lazyRmqSt.update(0,9,1);  // 5 4 9 5 8 4 10 9 4 3
    assert(lazyRmqSt.rmq(7,9) == 3);
    assert(lazyRmqSt.rmq(7,7) == 9);
    assert(lazyRmqSt.rmq(3,3) == 5);
    assert(lazyRmqSt.rmq(2,4) == 5);
    lazyRmqSt.update(0,2,-5);  // 0 -1 4 5 8 4 10 9 4 3
    assert(lazyRmqSt.rmq(0,3) == -1);

    printf("all test passed!\n");
    
    return 0;
}
