#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}


//----------------------------------------------------------------------//
#define MAX_SIZE 1000

class kmp{
private:
	string  P;
	int b[MAX_SIZE], m, n;
	void preprocess(){
		int i = 0, j = - 1;
		b[0] = -1;
		while(i < m){
			while(j >= 0 && P[i] != P[j])j = b[j];
			i++;j++;
			b[i] = j;
		}
	}
public:
	kmp(string p) :  P(p){
		m = p.size();
		preprocess();
	}
	int search(string T){
		int n = T.size();
		int i = 0, j = 0;
		while(i < n){
			while(j >= 0 && T[i] != P[j])j = b[j];
			i++;j++;
			if(j == m){
				return i - j;
				j = b[j];
			}
		}
		return -1;
	}
};

int main(){

	kmp s("ABABA");
	s.search("ACABAABABDABABABBACABABA");
	assert(s.search("ACABAABABDABABABBACABABA") == 10);
	printf("All test passed!\n");
	return 0;
}
