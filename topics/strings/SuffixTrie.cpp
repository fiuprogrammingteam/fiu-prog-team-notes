#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}
//----------------------------------------------------------------------//
#define R 300

class Node{
public :
	int suffix;
	Node * childs[R];
	Node(){
		suffix = -1;
	}
};

class Trie{

private :
	Node * root;
	char end_char;
	Node * add(Node * n, char * str, int i, int suffix){

		int pos = (int)str[i];

		if(n == NULL)
			n = new Node();

		if(str[i] == end_char){
			n->suffix = suffix;
		}else{
			n->childs[pos] = add(n->childs[pos],str, i + 1, suffix);
		}
		return n;
	}

public :
	Trie(){
		root = NULL;
		end_char = '$';
	}
	void add(char * str){
		int l = strlen(str);
		str[l++] = end_char;
		for (int i = 0; i < l; ++i) {
			root = add(root, str + i, 0, i);
		}
	}
};

int main(){
	printf("No test\n");
	return 0;
}
