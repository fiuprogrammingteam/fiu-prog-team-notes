#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

//----------------------------------------------------------------------//
#define MAX_N 1024

char T[MAX_N];
int SA[MAX_N], TempSA[MAX_N];
int RA[MAX_N], TempRA[MAX_N];
int LCP[MAX_N];

bool cmp(int a, int b){
	return strcmp(T + a, T + b) < 0;
}
class SuffixArrayNaive {
	int l;
public :
	SuffixArrayNaive(string str){
		strcpy(T, str.c_str());
		l = strlen(T);
		for (int i = 0; i < l; ++i) {
			SA[i] = i;
		}
		std::sort(SA, SA+l, cmp);
	}

	void debug(){
		for (int i = 0; i < l; ++i) {
			printf("SA[%d] = %d\n", i , SA[i]);
		}
	}
};

class SuffixArray {

	int n,m, c[MAX_N], Phi[MAX_N], PLCP[MAX_N];
	char P[MAX_N];
public :
	string lcp;
	SuffixArray(string str){
		n = (int)str.size();
		MEM(T,0);
		strcpy(T, str.c_str());
		constructSA();
		lcp = computeLCP();
	}

	void constructSA(){
		for (int i = 0; i < n; ++i) {
			SA[i] = i;
			RA[i] = (int)T[i];
		}
		int r;
		for (int k = 1; k < n ; k <<= 1) {
			countSort(k);
			countSort(0);

			TempRA[SA[0]] = r = 0;
			for (int i = 1; i < n; ++i) {
				TempRA[SA[i]] = (RA[SA[i]] == RA[SA[i-1]] && RA[SA[i] + k] == RA[SA[i-1] + k]) ? r : ++r;
			}

			for (int i = 0; i < n; ++i) {
				RA[i] = TempRA[i];
			}

			if(TempRA[SA[n-1]] == n-1)break;
		}
	}

	string computeLCP(){
		MEM(LCP,0);
		MEM(PLCP,0);
		MEM(Phi,0);
		Phi[SA[0]] = -1;
		for (int i = 1; i < n; ++i)
			Phi[SA[i]] = SA[i-1];
		int L = 0;
		for (int i = 0; i < n; ++i) {
			if(Phi[i] == -1){
				LCP[i] = 0;	continue;
			}
			while(T[i + L] == T[Phi[i] + L])L++;
			PLCP[i] = L;
			L = max(L-1,0);
		}
		string res;
		int best = 0;
		for (int i = 0; i < n; ++i) {
			LCP[i] = PLCP[SA[i]];
			if(LCP[i] > best){
				best = LCP[i];
				res = string(T + SA[i], T + SA[i] + LCP[i]);
			}
		}
		return res;
	}

	string getSufffix(int i){
		return string(T + SA[i]);
	}

	ii find(string str){
		strcpy(P, str.c_str());
		m = strlen(P);
		return stringMatching();
	}

	ii stringMatching(){

		int lo = 0,hi = n-1,mid=lo;

		while(lo < hi){
			mid = (hi + lo)/2;
			int cmp = strncmp(T+SA[mid],P,m);

			if(cmp >= 0)hi = mid;
			else lo = mid+1;
		}
		if(strncmp(T+SA[lo], P,m) != 0)return ii(-1,-1);

		ii res;
		res.first = lo;
		lo = 0, hi = n-1,mid=lo;

		while(lo < hi){
			mid = (hi + lo)/2;
			int cmp = strncmp(T+SA[mid],P,m);

			if(cmp > 0)hi = mid;
			else lo = mid+1;
		}
		if(strncmp(T+SA[hi], P,m) != 0)hi--;
		res.second = hi;
		return res;
	}

	void countSort(int k){
		int R = max(300, n);
		MEM(c,0);
		for (int i = 0; i < n; ++i)
			c[i + k < n ? RA[i + k] : 0]++;
		int sum = 0;
		for (int i = 0; i < R; ++i) {
			int t = c[i];
			c[i] = sum;
			sum += t;
		}

		for (int i = 0; i < n; ++i)
			TempSA[c[SA[i] + k < n ? RA[SA[i] + k] : 0]++] = SA[i];

		for (int i = 0; i < n; ++i)
			SA[i] = TempSA[i];
	}

	void debug(){
		printf("SA\t\t\t\tRA\n");
		for (int i = 0; i < n; ++i) {
			printf("SA[%d] = %d %*s\t\t%d\n", i , SA[i],n, T + SA[i], RA[SA[i]]);
		}
		printf("\n");
	}
};


int main(){
	printf("No test\n");
	return 0;
}
