#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}


//----------------------------------------------------------------------//
#define MAX_STRING 1000
int dp[MAX_STRING][MAX_STRING];

string A,B;

int match(char a, char b){
	return a == b ? 2 : -1;
}

int solve(int i ,int j){
	int &ret = dp[i][j];

	if(ret != -1)return ret;

	if( i == 0 && j == 0)
		ret = 0;
	else if(i == 0)
		ret = j * match(A[i], '-');
	else if(j == 0)
		ret = i * match('-',B[j]);
	else{

		int op1 = solve(i-1,j-1) + match(A[i], B[j]);	// score of match
		int op2 = solve(i-1, j) + match(A[i], '-');		// Delete Ai
		int op3 = solve(i , j-1) + match('-', B[j]);	// Insert Bj ( insert space in A)
		ret = max(op1,op2);
		ret = max(ret, op3);
	}

	return ret;
}

int main(){

	A = "ACAATCC";
	B = "AGCATGC";
	int l1 = (int)A.size();
	int l2 = (int)B.size();
	A = " " + A;
	B = " " + B;
	MEM(dp,-1);
	int score = solve(l1,l2);
	assert(score == 7);

	int i = l1;
	int j = l2;
	string ans1,ans2;
	ans1 = ans2 = "";
	while(i>0&&j>0){
		// delete Ai
		if(dp[i][j] == dp[i-1][j] - 1){
			ans1.push_back(A[i]);
			ans2.push_back('_');
			i--;
		}

		// mismatch
		if(dp[i][j] == dp[i-1][j-1] - 1){
			ans1.push_back(']');ans1.push_back(A[i]);ans1.push_back('[');
			ans2.push_back(']');ans2.push_back(B[j]);ans2.push_back('[');
			i--,j--;
		}

		// match
		if(dp[i][j] == dp[i-1][j-1] + 2){
			ans1.push_back(A[i]);
			ans2.push_back(B[j]);
			i--,j--;
		}

		// insert Bi
		if(dp[i][j] == dp[i][j-1] - 1){
			ans1.push_back('_');
			ans2.push_back(B[j]);
			j--;
		}
	}

	reverse(ans1.begin(), ans1.end());
	reverse(ans2.begin(), ans2.end());

	cout << ans1 << endl;
	cout << ans2 << endl;

	assert(ans1 == "A_CAAT[C]C");
	assert(ans2 == "AGCA_T[G]C");

	printf("All test passed!\n");
	return 0;
}
