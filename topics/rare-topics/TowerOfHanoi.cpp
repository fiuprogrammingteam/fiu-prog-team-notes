#include <bits/stdc++.h>
using namespace std;

#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;

/**
 * Problem description:
 * We are given a tower of eight disks,
 * initially stacked in decreasing size on one of three pegs
 *
 * The objective is to transfer the entire tower to one of the other pegs, moving
 * only one disk at a time and never moving a larger one onto a smaller.
 */

/**
 * open form,
 * recurrence relation
 * T(n) = 2*T(n)+1
 */
ll T_open(ll n){
	if(n == 0)return 0;
	return 2ll*T_open(n-1) + 1;
}

/**
 * Close form
 * Tn = 2^n+1
 */
ll T(ll n){
	return (1<<n)-1;
}

int main(){
    
	assert(T_open(1) == 1);
	assert(T_open(2) == 3);
	assert(T_open(3) == 7);

	assert(T(1) == 1);
	assert(T(2) == 3);
	assert(T(3) == 7);

	assert(T(6) == T_open(6));
	assert(T(10) == T_open(10));

	printf("All test passed!\n");
    return 0;
}
