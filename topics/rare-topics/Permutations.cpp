#include <cstdio>
#include <string.h>
using namespace std;
int level, N;
int array[100];
void print();
void permutations(int i );
bool next_permutation(int * arr, int n);
void swap(int * a, int *b);
int main(){
//    freopen("input/permutations.in", "r", stdin);
//
//    while(scanf("%d", &N) == 1){
//
//        memset(array , 0, sizeof array);
//        level = -1;
//        permutations(0);
//        printf("\n");
//    }
    printf("No test\n");
    return 0;
}


void print(){
    printf("%d", array[0]);
    for(int i = 1 ; i < N ; i++){
        printf(" %d", array[i]);
    }
    printf("\n");
}
void permutations(int i ){
    level++;
    
    array[i] = level;
    if(level == N){
        print();
    }else{
        for(int j = 0 ; j < N ; j++){
            if(array[j] == 0)
                permutations(j);
        }
    }
    level--;
    array[i] = 0;
}
void swap(int * a, int *b){
    int temp = *a;
    *a = *b;
    *b = temp;
}
bool next_permutation(int * arr, int n){
    int i,j;
    if(n == 1)return false;
    i = n-2;
    while(arr[i] > arr[i + 1] && i >= 0){
        i--;   
    }
    if(i == 0 )return false;
    while(arr[j] > arr[i + 1]){
        j--;   
    }
    swap(&arr[i], &arr[n]);
    
    return true;
}
