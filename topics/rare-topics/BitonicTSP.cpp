//
#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;
#define MAX_N 1000
double dp[MAX_N][MAX_N];
double dist[MAX_N][MAX_N];

double btsp(int p1, int p2){
	int v = p1 + p2 + 1;

	if(dp[p1][p2] < -0.5)
		return dp[p1][p2];

	return dp[p1][p2] = min(
			dist[p1][v] + btsp(v, p2),
			dist[v][p2] + btsp(p1, v));
}

int main(){
//	vector<pair<double, double> > points;
//	sort(points.begin(), points.end());
//
//	for (int i = 0; i < points.size(); ++i)
//		for (int j = i+1; j < points.size(); ++j)
//			dist[i][j] = dist[j][i]= hypot(points[i].first - points[j].first,points[i].second - points[j].second);
//
//	cout << btsp(0,0);
	printf("No test\n");
	return 0;
}
