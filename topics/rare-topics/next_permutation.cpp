#include <cstdio>
#include <string.h>
#include <algorithm>
using namespace std;

int array[100], N;
void print();
bool next_permutation(int * arr, int n);
void swap(int * a, int *b);

int main(){
//    freopen("input/next_permutations.in", "r", stdin);
//    memset(array , 0, sizeof array);
//    N = 0;
//    while(scanf("%d", array + N) == 1){
//        N++;
//    }
//     int m_try = 10;
//    do{
//
//            if(m_try-- < 0)break;
//        print();
//    }while(next_permutation(array, N));
	printf("No test\n");
    return 0;
}


void print(){
    printf("%d", array[0]);
    for(int i = 1 ; i < N ; i++){
        printf(" %d", array[i]);
    }
    printf("\n");
}

void swap(int * a, int *b){
    int temp = *a;
    *a = *b;
    *b = temp;
}

bool next_permutation(int * arr, int n){
    int i,j, pivot;
    if(n == 1)return false;
    i = n-2;
    while(i >= 0 && arr[i] >= arr[i + 1]){
        i--;   
    }
    
    if(i < 0 )return false;
    pivot = arr[i];
    j = n-1;
    while(arr[j] <= pivot){
        j--;   
    }
    
    swap(&arr[i], &arr[j]);
    sort(arr + i + 1 , arr + n);
    


    return true;
}
