//
#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

#define N 100000
#define SQN (334)

/**
 * Problem: count the values in the range (l,r) which are repeated at least two times
 * http://blog.anudeep2011.com/mos-algorithm/
 */

int n,q;
vector<pair<ii, int > > queries;
vi A;
int C[N];
int ans;

bool cmp(pair<ii,int> a, pair<ii,int> b){
	if(a.first.first /  SQN != b.first.first / SQN)return  a.first.first /  SQN < b.first.first / SQN;
	return a.first.second < b.first.second;
}

void add(int i){
	C[A[i]]++;
	if(C[A[i]] == 2)
		ans++;
}

void remove(int i){
	C[A[i]]--;
	if(C[A[i]] == 1)
		ans--;
}

vi Mo(){
	ans = 0;

	sort(queries.begin(), queries.end(), cmp);

	int current_left = 0;
	int current_right = 0;

	vi Ans(queries.size(), 0);
	MEM(C,0);

	for (int i = 0; i < queries.size(); ++i) {
		int l = queries[i].first.first;
		int r = queries[i].first.second;
		int j = queries[i].second;
		while(current_left<l)
			remove(current_left++);

		while(current_left>l)
			add(current_left--);

		while(current_right<r)
			add(current_right++);

		while(current_right>r)
			remove(current_right--);

		Ans[j] = ans;
 	}
	return Ans;
}

void test1();
int main(){
	test1();
	printf("All test passed!\n");
	return 0;
}
void test1(){
	n = 8;
	q = 4;
	int B[] = {2,2,3,5,7,2,5,9};
	int L[] ={0,1,0,1};
	int R[] ={2,2,8,7};
	int exp[] = {1,0,2,2};
	A.clear();
	queries.clear();
	A.assign(B, B+n);
	for (int i = 0; i < q; ++i) {
		queries.push_back(pair<ii,int>(ii(L[i],R[i]),i));
	}
	vi sol = Mo();
	assert(sol == vi(exp,exp+q));
}
