#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}
//----------------------------------------------------------------------//
vector<vii> g;
list<int> tour;
/**
 * An Euler tour (or Eulerian tour) in an undirected graph is a
 * tour that traverses each edge of the graph exactly once.
 *
 * An undirected graph has a closed Euler tour iff it is connected and each vertex has an even degree.
 *
 * An undirected graph has an open Euler tour iff it is connected, and each vertex, except for exactly two vertices,
 * has an even degree.
 * The two vertices of odd degree have to be the endpoints of the tour.
 *
 */
void get_euler_tour(list<int>::iterator i, int u){
	for(int j = 0 ; j < g[u].size() ; j++){
		ii& v = g[u][j];
		if(v.second){
			v.second = 0;
			for(int k = 0 ; k < g[v.first].size() ; k++){
				ii& w = g[v.first][k];
				if(w.first == u && w.second){
					w.second = 0;
					break;
				}
			}
			get_euler_tour(tour.insert(i, u), v.first);
		}
	}
}

int main(){

	// Read a graph
	int V,E;
//	scanf("%d %d", &V, &E);
//	g.assign(V, vii());
//	for(int i = 0 ; i < E ; i++){
//		int u,v;
//		scanf("%d %d", &u, &v);
//		g[u].push_back(ii(v,1));
//		g[v].push_back(ii(u,1));
//	}
//
//	tour.clear();
//	get_euler_tour(tour.begin(), 0);
//
//	printf("euler tour:");
//	for(list<int>::iterator it = tour.begin() ;it != tour.end(); it++)printf(" %d", *it);
//	printf("\n");

	printf("No test\n");

	return 0;
}
