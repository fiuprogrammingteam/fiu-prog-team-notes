#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

//----------------------------------------------------------------------//

class Edge{
	int u,v;
public :
	Edge(int _u, int _v, double wei) : u(_u), v(_v), weight(wei){
	}
	double weight;
	int from(){
		return u;
	}
	int to(){
		return v;
	}
	int other(int w){
		if(w == u)return v;
		if(w == v)return u;
		return -1;
	}
};

typedef vector<Edge> edgeList;

priority_queue<ii, vector<ii >, std::greater<ii > > q;
vector<double> dist;
vi vis;

void relax(vector<edgeList> G, int u){
	for (int i = 0; i < G[u].size(); ++i) {
		Edge e = G[u][i];
		int v = e.other(u);
		if(!vis[v] && dist[u] + e.weight< dist[v]){
			dist[v] = dist[u] + e.weight;
			q.push(ii(-dist[v], v));
		}
	}
}

int main(){
//	freopen("input1.txt", "r", stdin);
//	int V,E,Src, Dest;
//	scanf("%d %d", &V, &E);
//	scanf("%d %d", &Src, &Dest);
//
//	vector<edgeList> G;
//	G.assign(V+1, edgeList());
//	for (int i = 0; i < E; ++i) {
//		int u,v;
//		double w;
//		scanf("%d %d %lf", &u, &v, &w);
//		Edge e(u,v,w);
//		G[e.from()].push_back(e);
//		G[e.to()].push_back(e);
//	}
//
//	dist.assign(V+1, INF);
//	vis.assign(V+1, 0);
//
//	q.push(ii(0,Src));
//	dist[Src] = 0;
//	while(!q.empty()){
//		ii u = q.top();
//		q.pop();
//		double d = -u.first;
//		if(d > dist[u.second])continue;
//		relax(G, u.second);
//	}
//	printf("Dist = %lf\n", dist[Dest]);
	return 0;
}
