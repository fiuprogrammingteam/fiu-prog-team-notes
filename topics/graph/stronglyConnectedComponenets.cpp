#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}
//----------------------------------------------------------------------//
#define UNVISITED -1

vector<vi> g;
vi dfs_parent;
vi dfs_num;
vi dfs_lo;
vi visited;
vi S;

int dfsNumberCounter, numSCC;

/**
 * a graph is said to be strongly connected if every vertex is reachable from every other vertex.
 */
void tarjansSCC(int u){
	dfs_num[u] = dfs_lo[u] = dfsNumberCounter++;
	S.push_back(u);
	visited[u] = true;
	for(int i = 0 ; i < g[u].size() ; i++){
		int v = g[u][i];

		if(dfs_num[v] == UNVISITED)
			tarjansSCC(v);
		if(visited[v])
			dfs_lo[u] = min(dfs_lo[u], dfs_lo[v]);
	}

	if(dfs_num[u] == dfs_lo[u]){
		printf("SSC %d:", ++numSCC);
		while(1){
			int v = S.back();
			S.pop_back();
			visited[v] = false;
			printf(" %d", v);
			if(v == u)break;
		}
		printf("\n");
	}
}

/**
 *  Kosaraju’s algorithm.
 */
vi stk;
void KosarajuDfs(vector<vi> _g, int u, bool printV){
	visited[u] = true;
//	if(printV)printf("%d ",u);
	for (int i = 0; i < _g[u].size(); ++i) {
		int v = _g[u][i];
		if(!visited[v]){
			KosarajuDfs(_g, v, printV);
		}
	}
	stk.push_back(u);
}

vector<vi> gT(){
	vector<vi> gt;
	gt.assign(g.size(), vi());
	for (int u = 0; u < g.size(); ++u) {
		for (int i = 0; i < g[u].size(); ++i) {
			int v = g[u][i];
			gt[v].push_back(u);
		}
	}

	return gt;
}

int main(){
	// TODO Add test
	printf("No test\n");
	return 0;
}
