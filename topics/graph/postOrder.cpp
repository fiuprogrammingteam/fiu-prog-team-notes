#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

//----------------------------------------------------------------------//
int N;
int idx;

/*
 * Given the in-order and pre-order traversal of a tree
 * outputs the post-order traversal
 */
void postOrder(vii t, ii p){

	int node_inorder_idx = t[idx].first;
	int node_val = t[idx].second;

	// Go left
	if(idx < t.size()-1 && t[idx + 1].first < node_inorder_idx && t[idx + 1].first > p.first){
		idx++;
		postOrder(t,ii(p.first,node_inorder_idx));
	}

	// Go right
	if(idx < t.size()-1 && t[idx + 1].first > node_inorder_idx && t[idx + 1].first < p.second){
		idx++;
		postOrder(t,ii(node_inorder_idx, p.second));
	}

	// Output root
	printf("%d ", node_val);
}

int main(){

	// TODO add test
	printf("No test\n");
	return 0;
}
