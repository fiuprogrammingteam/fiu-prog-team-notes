#include <bits/stdc++.h>
using namespace std;

const int  min_int = std::numeric_limits<int>::min();
const int max_int = std::numeric_limits<int>::max();

#define max(a,b)a>b?a:b
#define min(a,b)a<b?a:b
#define MEM(arr,val)memset(arr,val, sizeof arr)
#define inf (1<<30)
#define PI acos(0)*2.0
#define eps 1.0e-9
#define iszero(a)fabs(a)<eps

typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int gcd(int a,int b){if(b == 0)return a;return gcd(b,a%b);}

//----------------------------------------------------------------------//
#define UNVISITED -1

vector<vii> g;
vi dfs_parent;
vi dfs_num;
vi dfs_lo;
vi articulation_vertex;
int dfsNumberCounter, dfsRoot, rootChildren;

void dfs(int u){
	dfs_num[u] = dfs_lo[u] = dfsNumberCounter++;

	for(int i = 0 ; i < g[u].size() ; i++){
		ii v = g[u][i];
		if(dfs_num[v.first] == UNVISITED){

			dfs_parent[v.first] = u;

			if(u == dfsRoot)rootChildren++;

			dfs(v.first);

			if(dfs_lo[v.first] >= dfs_num[u]){
				articulation_vertex[u] = true;
			}
			if(dfs_lo[v.first] > dfs_num[u]){
				printf("%d %d\n", u , v.first);
			}
			dfs_lo[u] = min(dfs_lo[u],dfs_lo[v.first]);
		}else if(dfs_parent[u] != v.first){
			dfs_lo[u] = min(dfs_lo[u], dfs_num[v.first]);
		}
	}
}

int main(){
//	int V,E,i,j;
//	scanf("%d %d", &V, &E);
//	g.assign(V, vii());
//	dfs_parent.assign(V,0);
//	dfs_num.assign(V,UNVISITED);
//	dfs_lo.assign(V,0);
//	articulation_vertex.assign(V,0);
//	dfsNumberCounter = 0;
//
//	// Read undirected edges
//	FOR(i,0,E){
//		int u,v;
//		scanf("%d %d", &u,&v);
//		g[u].push_back(ii(v,0));
//		g[v].push_back(ii(u,0));
//	}
//
//	printf("Briges:\n");
//	FOR(i,0, V){
//		if(dfs_num[i] == UNVISITED){
//			dfsRoot = i;
//			rootChildren = 0;
//			dfs(dfsRoot);
//			articulation_vertex[dfsRoot] = ( rootChildren > 1);
//		}
//	}
//
//	printf("Articulation vertices:\n");
//	FOR(i,0, V){
//		if(articulation_vertex[i]){
//			printf("%d\n", i);
//		}
//	}
	printf("No test\n");
	return 0;
}
