#include <bits/stdc++.h>
using namespace std;

const int  INF = std::numeric_limits<int>::max()/3;

#define max(a,b)(a>b?a:b)
#define min(a,b)(a<b?a:b)
#define MEM(arr,val)memset(arr,val, sizeof arr)
#define PI (acos(0)*2.0)
#define eps 1.0e-9
#define are_equal(a,b)fabs(a-b)<eps
#define LS(b)(b&(-b)) // Least significant bit

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

// ---------------------------------------------------------------------------------------


class Edge{
public:
	int u,v,cap,f;

	Edge(int _u, int _v, int _cap) : u(_u),v(_v),cap(_cap){
		f = 0;
	}
	int other(int vertex){
		if(vertex == u)return v;
		else if(vertex == v)return u;
		return -1;
	}

	int residualTo(int vertex){
		if(vertex == u)return f;
		return cap - f;
	}

	void addFlowTo(int vertex, int ff){
		if(vertex == u){
			f -= ff;
		}else if(vertex == v){
			f += ff;
		}
	}
};


typedef vector<Edge*> edgeList;

#define MAX_V 100000
#define MAX_E 100000
const int SRC = 0;
const int SINK = 1;
int Flow;
edgeList parent;
vi visited;
vector<edgeList> adj;

void addEdge(int u, int v, int cap){
	Edge* e = new Edge(u,v,cap);
	adj[u].push_back(e);
	adj[v].push_back(e);
}

bool augmentingPath(){
	queue<int> q;
	parent.assign(MAX_V, NULL);
	visited.assign(MAX_V, 0);

	q.push(SRC);
	visited[SRC] = 1;

	while(!q.empty()){
		int u = q.front();
		q.pop();
		if(u == SINK)
			break;
		for (int i = 0; i < (int)adj[u].size(); ++i) {
			Edge* e = adj[u][i];
			int v = e->other(u);
			if(!visited[v] && e->residualTo(v) > 0){
				visited[v] = 1;
				parent[v] = e;
				q.push(v);
			}
		}
	}
	return visited[SINK];
}

// Ford Fulkerson
int maxFlow(){
	int mf = 0;
	while(augmentingPath()){
		Flow = 1e9;

		for(int v = SINK; v != SRC ; v = parent[v]->other(v)){
			Flow = min(Flow, parent[v]->residualTo(v));
		}

		for(int v = SINK; v != SRC ; v = parent[v]->other(v)){
			parent[v]->addFlowTo(v,Flow);
		}
		mf += Flow;
	}
	return mf;
}


void data1();

int main(){
	 data1();
//	 printf("%d\n", maxFlow());
	 assert(maxFlow() == 4);
	 printf("All test passed\n");
}


void data1(){

	adj.assign(MAX_V, edgeList());

	// MF should be 4
	addEdge(SRC,2,2);
	addEdge(SRC,3,3);
	addEdge(2,4,5);
	addEdge(2,5,1);
	addEdge(4,SINK,1);
	addEdge(5,SINK,1);
	addEdge(3,SINK,2);
}
