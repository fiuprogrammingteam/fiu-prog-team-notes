#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

//----------------------------------------------------------------------//
#define MAX_SIZE 300
vector<vii> g;
long mat[MAX_SIZE][MAX_SIZE];
int p[MAX_SIZE][MAX_SIZE];

int V,E,Q,i,j,k;
/**
 * This outputs the path
 */

// Floyd Warshall
void apsp(){
	for (int k = 0; k < V; ++k) {
		for (int i = 0; i < V; ++i) {
			for (int j = 0; j < V; ++j) {
				if(mat[i][k] + mat[k][j] < mat[i][j]){
					mat[i][j] = mat[i][k] + mat[k][j];
					p[i][j] = p[k][j];
				}
			}
		}
	}
}

void print_path(int i , int j){
	if(i != j)print_path(i, p[i][j]);
	printf(" %d", j);
}
int getShortestPath(int u, int v){
	return mat[u][v];
}

int main(){

//	scanf("%d %d", &V, &E);
//	// Init parent array and adj matrix
//	g.assign(V, vii());
//	for (int i = 0; i < V; ++i) {
//		for (int j = 0; j < V; ++j) {
//			p[i][j] = i;
//			mat[i][j] = 1e9;
//		}
//	}
//	// Load undirected edges
//	for (int i = 0; i < E; ++i) {
//		int u,v,w;
//		scanf("%d %d %d", &u,&v, &w);
//		g[u].push_back(ii(v,0));
//		g[v].push_back(ii(u,0));
//		mat[u][v] = mat[v][u] = w;
//	}
//	// Floyd Warshall
//	// Find all pairs shortest paths
//
//	// READ queries
//	scanf("%d", &Q);
//	while(Q--){
//		int u, v;
//		scanf("%d %d", &u, &v);
//		printf("(%d,%d)\n", u,v);
//		printf("path:");
//		print_path(u,v);
//		printf("\ncost: %ld\n", mat[u][v]);
//	}
	printf("No test\n");
	return 0;
}
