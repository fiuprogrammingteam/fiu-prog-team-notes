#include <bits/stdc++.h>
using namespace std;

const int  INF = std::numeric_limits<int>::max()/3;

#define max(a,b)(a>b?a:b)
#define min(a,b)(a<b?a:b)
#define MEM(arr,val)memset(arr,val, sizeof arr)
#define PI (acos(0)*2.0)
#define eps 1.0e-9
#define are_equal(a,b)fabs(a-b)<eps
#define LS(b)(b&(-b)) // Least significant bit

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

// ---------------------------------------------------------------------------------------
int n,idx;
vi E,L,H,st;
vector<vi> G;

void swap(int &u,int &v){
	int t=u;u=v;v=t;
}

void build(int p, int l, int r){

	if(l == r){
		st[p] = l;
	}else{

		build(p << 1, l, (l+r)/2);
		build((p << 1)+1,(l+r)/2+1, r);

		int p1 = st[p<<1];	// min idx on left
		int p2 = st[(p<<1)+1];

		st[p] = (L[p1] < L[p2]) ? p1 : p2;
	}
}

void build(){
	st.assign(idx*4, 0);
	build(1, 0, idx-1);
}

int rmq(int p, int l, int r, int x, int y){

	if(r < x || l > y)return -1;
	if(l>=x && r <= y)return st[p];

	int p1 = rmq(p<<1		,l,(l+r)/2,x,y);
	int p2 = rmq((p<<1)+1	,(l+r)/2+1,r,x,y);

	if(p1 == -1)return p2;
	if(p2 == -1)return p1;

	return L[p1] < L[p2] ? p1 : p2;
}

int rmq(int x, int y){
	return rmq(1, 0, idx-1, x, y);
}

void dfs(int u, int depth){

	H[u] = idx;
	E[idx] = u;
	L[idx++] = depth;

	for (int i = 0; i < G[u].size(); ++i) {
		int v = G[u][i];
		dfs(v, depth + 1);
		E[idx] = u;
		L[idx++] = depth;
	}
}

void addEdge(int u, int v){
	G[u].push_back(v);
}

class naiveLCA{
public:
	int N;
	vector<vi> _G;
	vi P,h;

	naiveLCA(vector<vi> _g){
		N = _g.size()+1;
		_G = _g;
		P.assign(N+1, -1);
		h.assign(N+1, 0);
		dfs(0);
	}

	void dfs(int u, int p = -1){
		if(p != -1){
			h[u] = h[p] + 1;
		}
		P[u] = p;
		for (int i = 0,v; i < _G[u].size(); ++i) {
			v = _G[u][i];
			if(v != p) dfs(v,u);
		}
	}

	int LCA(int u, int v){
		if(u == v)return u;
		if(h[u] < h[v])swap(u,v);
		return LCA(P[u],v);
	}
};
class logLCA{
public:
	int MAXLOG ;
	int N;
	vector<vi> _G;
	vi h;
	vector<vi> P;
	logLCA(vector<vi> _g){
		MAXLOG = 30;
		N = _g.size()+1;
		_G = _g;
		P.assign(N+1, vi(MAXLOG,-1));
		h.assign(N+1, 0);
		dfs(0);
	}

	void dfs(int u, int p = -1){
		if(p != -1)
			h[u] = h[p] + 1;

		P[u][0] = p;

		for (int i = 1; i < MAXLOG; ++i)
			if(P[u][i-1] != -1)
				P[u][i] = P[P[u][i-1]][i-1];

		for (int i = 0,v; i < _G[u].size(); ++i) {
			v = _G[u][i];
			if(v != p) dfs(v,u);
		}
	}

	int LCA(int u, int v){

		if(h[v]<h[u])swap(u,v);
		for(int i = MAXLOG -1 ; i >= 0 ;i--)
			if(P[v][i] != -1 && h[P[v][i]] >= h[u])
				v = P[v][i];

		// how h[v] == h[u]
		if(v == u)return v;
		for(int i = MAXLOG -1 ; i >= 0 ;i--)
			if(P[v][i]!=P[u][i])
				v = P[v][i],u=P[u][i];
		return P[v][0];
	}

};


void testLogLCA(){

	logLCA test(G);
//	printf("testLogLCA: \n");
//	printf("%d\n", test.LCA(3,5)); // 1
//	printf("%d\n", test.LCA(4,6)); // 0
//	printf("%d\n", test.LCA(3,6)); // 0
//	printf("%d\n", test.LCA(3,4)); // 3
	assert(test.LCA(3,5) == 1);
	assert(test.LCA(4,6) == 0);
	assert(test.LCA(3,6) == 0);
//	printf("%d \n", test.LCA(3,4) );
	assert(test.LCA(3,4) == 3);
}
void testnaiveLCA(){
	naiveLCA test(G);
//	printf("test naive: \n");
//	printf("%d\n", test.LCA(3,5)); // 1
//	printf("%d\n", test.LCA(4,6)); // 0
//	printf("%d\n", test.LCA(3,6)); // 0
//	printf("%d\n", test.LCA(3,4)); // 3
	assert(test.LCA(3,5) == 1);
	assert(test.LCA(4,6) == 0);
	assert(test.LCA(3,6) == 0);
	assert(test.LCA(3,4) == 3);
}

int main(){
	n = 7;
	G.assign(n+1, vi());
	E.assign(2*n+1, 0);
	L.assign(2*n+1, INF);
	H.assign(n+1, 0);

	addEdge(0,1);
	addEdge(0,2);
	addEdge(2,6);
	addEdge(1,3);
	addEdge(1,5);
	addEdge(3,4);

	////////
	//First method
	idx = 0;
	dfs(0,0);
	build();
	assert( E[rmq(H[3],H[5])] == 1);
	////////

	testnaiveLCA();
	testLogLCA();

	printf("All test passed\n");
}

void data1(){

}
