#include <bits/stdc++.h>
using namespace std;

const int  INF = std::numeric_limits<int>::max()/3;

#define max(a,b)(a>b?a:b)
#define min(a,b)(a<b?a:b)
#define MEM(arr,val)memset(arr,val, sizeof arr)
#define PI (acos(0)*2.0)
#define eps 1.0e-9
#define are_equal(a,b)fabs(a-b)<eps
#define LS(b)(b&(-b)) // Least significant bit

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;


// ---------------------------------------------------------------------------------------
#define MAX_V 100
#define MAX_E 100


class Edge{
public:
	int u,v,cap,f,cost;

	Edge(int _u, int _v, int _cap, int _c) : u(_u),v(_v),cap(_cap),cost(_c){
		f = 0;
	}

	int getCost(int w){
		if(w == u)return - cost;
		return cost;
	}

	int other(int vertex){
		if(vertex == u)return v;
		else if(vertex == v)return u;
		return -1;
	}

	int residualTo(int vertex){
		if(vertex == u)return f;
		return cap - f;
	}

	void addFlowTo(int vertex, int ff){
		if(vertex == u){
			f -= ff;
		}else if(vertex == v){
			f += ff;
		}
	}
};


typedef vector<Edge*> edgeList;

int SRC = 0;
int SINK = 1;
int Flow;
edgeList parent;
vi visited;
vector<edgeList> adj;
set<int> vertices;
vi dist;

void addEdge(int u, int v, int cap, int cost){
	Edge* e = new Edge(u,v,cap,cost);
	adj[u].push_back(e);
	adj[v].push_back(e);
	vertices.insert(u);
	vertices.insert(v);
}

// Bellman Ford Algorithm
bool SSSP(){
	dist.assign(SINK+1,INF);
	dist[SRC] = 0;
	parent.assign(SINK+1 , NULL);
	for (int i = 0; i < vertices.size() -1; ++i) {
		for (set<int>::iterator it = vertices.begin() ; it != vertices.end() ; ++it) {
			int u = *it;
			if(dist[u] != INF)
				for (int k = 0; k < (int)adj[u].size(); ++k) {
					Edge *e = adj[u][k];
					int v = e->other(u);
					if(e->residualTo(v) > 0 && dist[u] + e->getCost(v) < dist[v]){
						parent[v] = e;
						dist[v] = dist[u] + e->getCost(v);
					}
				}
		}
	}

	return dist[SINK] != INF;
}


// Ford Fulkerson
int maxFlow(){

	for (int i = 0; i < adj.size(); ++i) {
		for (int j = 0; j < adj[i].size(); ++j) {
			adj[i][j]->f = 0;
		}
	}

	int mf = 0;
	while(SSSP()){
		Flow = INF;

		for(int v = SINK; v != SRC ; v = parent[v]->other(v)){
			Flow = min(Flow, parent[v]->residualTo(v));
		}

		for(int v = SINK; v != SRC ; v = parent[v]->other(v)){
			parent[v]->addFlowTo(v,Flow);
		}
		mf += Flow;
	}
	return mf;
}


void data1();

int main(){
	 data1();
//	 printf("%d\n", maxFlow());
	 assert( maxFlow() == 4);
	 printf("All test passed\n");
}


void data1(){

	adj.assign(MAX_V, edgeList());

	// MF should be 4
	addEdge(SRC,2,2,1);
	addEdge(SRC,3,3,1);
	addEdge(2,4,5,1);
	addEdge(2,5,1,1);
	addEdge(4,SINK,1,1);
	addEdge(5,SINK,1,1);
	addEdge(3,SINK,2,1);
}


// NOTES

/*
 * Minimum cost flow problem
 *
 * minimize sum of ( Cij*Xij )
 *
 * Xij = units (flow) node_i to node_j
 * Cij = cost per unit of flow
 */
