#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

//----------------------------------------------------------------------//

vector<vii> g;
vi taken;
priority_queue<ii> q;
void process(int u){
	taken[u] = 1;

	for(int i = 0 ; i < g[u].size() ; i++){
		ii v = g[u][i];
		if(!taken[v.first]){
			q.push(ii(-v.second, -v.first));
		}
	}
}

int main(){
//	int V,E,Q,u,v,w,i,j;
//	scanf("%d %d", &V, &E);
//
//	// Init global variables
//	g.assign(V, vii());
//	taken.assign(V, 0);
//
//	// Load graph
//	for (int i = 0; i < E; ++i) {
//		scanf("%d %d %d", &u,&v, &w);
//		g[u].push_back(ii(v,w));
//		g[v].push_back(ii(u,w));
//	}
//
//	// Prim's MPT algorithm
//	process(0);
//	int st_cost = 0;
//	while(!q.empty()){
//		ii top = q.top();q.pop();
//		int u = -top.second;
//		int w = -top.first;
//
//		if(!taken[u])
//			st_cost += w, process(u);
//	}
//
//	printf("mst cost = %d\n", st_cost);
	printf("No test\n");
	return 0;
}
