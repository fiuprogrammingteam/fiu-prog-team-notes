#include <bits/stdc++.h>
using namespace std;

#define max(a,b)(a>b?a:b)
#define min(a,b)(a<b?a:b)
#define MEM(arr,val)memset(arr,val, sizeof arr)
#define PI acos(0)*2.0
#define eps 1.0e-9
#define LS(b)(b& (-b)) // Least significant bit
#define DEG_to_RAD(a)((a*PI)/180.0) // convert to radians

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

int gcd(int a,int b){return b == 0 ? a : gcd(b,a%b);}
int lcm(int a,int b){return a*(b/gcd(a,b));}

//----------------------------------------------------------------------//
struct point{
	double x,y;
	point(){x = y = 0.0;}
	point(double _x, double _y) : x(_x),y(_y){	}
	bool operator < (point other) const {
		if(fabs(x-other.x)>eps)
			return x < other.x;
		return y < other.y;
	}
	bool operator == (point other) const {
		return fabs(x-other.x) < eps && fabs(y-other.y)<eps;
	}
};

struct vec{
	double x,y;
	vec(){x = y = 0.0;}
	vec(double _x, double _y) : x(_x),y(_y){}
	vec(point p1, point p2) : x(p2.x-p1.x),y(p2.y-p1.y){}
	vec scale(double s){
		return vec(this->x * s,this->y * s);
	}
};

/**
 * a*x + b*y + c = 0
 * a = -slope
 * c = -(y-intercept)
 */
struct line{
	double a,b,c;
	line(){a=b=c=0.0;}
	line(double _a,double _b,double _c): a(_a),b(_b),c(_c){}
	line(point p1, point p2){
		if(fabs(p1.x-p2.x) < eps){
			a = 1.0;
			b = 0.0;
			c = -p1.x;
		}else{
			a = -(double)(p1.y - p2.y)/(p1.x - p2.x);// slope
			b = 1.0;
			c = -(double)(a*p1.x)-p1.y;
		}
	}
};

vector<point> toPoly(int sz,...){
	va_list vl;
	va_start(vl,sz);
	vector<point> Q;
	for (int i=1;i<sz;i++)
	{
		point val=va_arg(vl,point);
		Q.push_back(val);
	}
	va_end(vl);
	return Q;
}

double cross(vec a, vec b){return a.x*b.y - a.y*b.x;}
double dot(vec a, vec b){return a.x * b.x + a.y * b.y;}
double norm_sq(vec a){return a.x * a.x + a.y * a.y;}
double dist(point p1, point p2){return hypot(p1.x - p2.x, p1.y- p2.y);}
double perimeter(double ab, double bc, double ca){return ab + bc + ca;}

// Heron's formula
double trigArea(double ab, double bc, double ca){
	double s = perimeter(ab,bc,ca)/2.0;
	return sqrt(s*(s-ab)*(s-bc)*(s-ca));
}

double trigArea(point p, point q, point r){
	return trigArea(dist(p,q), dist(q,r), dist(r,p));
}
vec toVec(point a, point b){
	return vec(b.x - a.x, b.y - a.y);
}
// returns true if point r is on the same line as the line pq
bool collinear(point p, point q, point r){
	return fabs(cross(vec(p,q),vec(p,r)) )<  eps;
}
// Polygon
vector<point> P0;

double perimeter(const vector<point> &P){
	double d = 0.0;
	for (int i = 0; i < ((int)P.size()) - 1; ++i)
		d += dist(P[i], P[i+1]);
	return d;
}

bool ccw(point p, point q, point r, bool acceptCollinear = false){
//	return (acceptCollinear  && collinear(p,q,r)) || cross(vec(p,q),vec(p,r)) > 0;
	return ( collinear(p,q,r)) || cross(vec(p,q),vec(p,r)) > 0;
}

// oa*ob = |oa|*|ob|*cos(theta), theta = arccos(oa*ob / (|oa|*|ob|) )
double angle(point a, point o, point b){
	vec oa(o,a), ob(o,b);
	return acos(dot(oa,ob)/ sqrt(norm_sq(oa)*norm_sq(ob)));
}

double area(vector<point> P){
	double a = 0.0,x1,x2,y1,y2;
	for (int i = 0; i < P.size()-1; ++i) {

		x1 = P[i].x;x2 = P[i+1].x;
		y1 = P[i].y;y2 = P[i+1].y;
		a += (x1*y2 - x2*y1);
	}
	return fabs(a)/2.0;
}

bool isConvex(const vector<point> &P){
	int sz = P.size();
	if(sz <= 3)return false;
	bool isLeft = ccw(P[0],P[1],P[2]);
	for (int i = 1; i < sz-1; ++i) {

		int idx1 = i;
		int idx2 = i+1;
		int idx3 = (i+2 == sz) ? 1 : i +2;

		// collinear points
		if(collinear(P[idx1],P[idx2],P[idx3]))continue;

		if(ccw(P[idx1],P[idx2],P[idx3]) != isLeft)return false;
	}
	return true;
}

bool inPolygon(point pt, const vector<point> &P){
	if((int)P.size() == 0)return false;
	double sum = 0.0;
	for (int i = 0; i < (int)P.size()-1; ++i) {
		if(ccw(pt,P[i],P[i+1]))
			sum += angle(P[i],pt,P[i+1]);
		else
			sum -= angle(P[i],pt,P[i+1]);

	}
	return fabs(fabs(sum) - 2*PI) < eps;
}

void swap(double &a, double &b){
	double tmp = a;
	a = b;
	b = tmp;
}
bool contained(point a,point b, point c){
	double x1 = a.x,
			y1 = a.y,
			x2 = b.x,
			y2 = b.y;
	if(x1 > x2){
		swap(x1,x2);
		swap(y1,y2);
	}

	return (c.x >= x1-eps && c.x <= x2+eps) && (c.y >= y1-eps && c.y <= y2+eps);
}

/**
 * Check is two line segments intercept using the left turn test
 */
bool seq_intersect(point a, point b, point c, point d){
	bool ans = (collinear(a,b,c) && contained(a,b,c)) ||
			(collinear(a,b,d) && contained(a,b,d)) ||
			(collinear(c,d,a) && contained(c,d,a)) ||
			(collinear(c,d,b) && contained(c,d,b));
	return ans || (ccw(a,b,c) != ccw(a,b,d) && ccw(c,d,a) != ccw(c,d,b));
}

/**
 * Checks if two polygons intercept
 */
bool polyIntercept(const vector<point> &P1, const vector<point> &P2){

	// Test if a point of a polygon is inside the other polygon
	for (int i = 0; i < (int)P1.size(); ++i)
		if(inPolygon(P1[i], P2))return true;
	for (int i = 0; i < (int)P2.size(); ++i)
		if(inPolygon(P2[i], P1))return true;

	// Test if a line segment of one of the polygons intercepts a line segment in the other polygon
	for (int i = 0; i < (int)P1.size()-1; ++i)
		for (int j = 0; j < (int)P2.size()-1; ++j)
			if(seq_intersect(P1[i], P1[i+1],P2[j], P2[j+1]))return true;

	// Otherwise the polygons don't intercept
	return false;
}


point lineInterceptSeg(point p, point q, point A, point B){
	double a = B.y - A.y;
	double b = A.x - B.x;
	double c = B.x*A.y - A.x*B.y;

	double u = fabs(a*p.x + b*p.y + c);
	double v = fabs(a*q.x + b*q.y + c);
	return point((p.x * v + q.x * u)/(u+v), (p.y * v + q.y * u)/(u+v));
}

vector<point> cutPolygon(point a, point b, const vector<point> &Q){
	vector<point> P;

	for (int i = 0; i < (int)Q.size(); ++i) {
		double left1 = cross(vec(a,b), vec(a,Q[i])), left2 = 0.0;
		if(i != (int)Q.size()-1)left2 = cross(vec(a,b), vec(a,Q[i+1]));

		if(left1  > -eps)P.push_back(Q[i]);
		if(left1*left2 < -eps)
			P.push_back(lineInterceptSeg(Q[i],Q[i+1],a,b));
	}
	if(!P.empty() && !(P.back() == P.front()))
		P.push_back(P.front());
	return P;
}
point pivot;
bool angleCmp(point a, point b){
	if(collinear(pivot,a,b))
		return dist(pivot, a) < dist(pivot,b);
	double d1x = a.x - pivot.x - eps, d1y = a.y - pivot.y + eps;
	double d2x = b.x - pivot.x - eps, d2y = b.y - pivot.y + eps;
	return (atan2(d1y,d1x) - atan2(d2y,d2x)) < 0;
}

vector<point> CH(vector<point> P){

	int i,j,n = P.size();
	if(n <= 3){
		if(!(P[0] == P[n-1]))P.push_back(P[0]);
		return P;
	}

	int P0 = 0;
	for (i = 1 ; i < n; ++i)
		if(P[i].y < P[P0].y || (fabs(P[i].y-P[P0].y) < eps && P[i].x > P[P0].x))
			P0 = i;

	point temp = P[0];P[0]=P[P0]; P[P0] = temp;

	pivot = P[0];
	sort(++P.begin(), P.end(), angleCmp);

	vector<point> S;
	S.push_back(P[n-1]);S.push_back(P[0]);S.push_back(P[1]);

	i = 2;
	while(i < n){
		j = (int)S.size()-1;
		if(ccw(S[j-1], S[j], P[i], true))S.push_back(P[i++]);
		else S.pop_back();
	}
	return S;
}

void RotatingCaliphers(vector<point> P){

	vector<point> Q;
	for (int i = 0; i < P.size()-1; ++i) {
		Q.push_back(P[i]);
	}

	P = CH(Q);
	reverse(P.begin(), P.end());

	for (int i = 0; i < P.size(); ++i) {
		printf("%lf %lf\n", P[i].x, P[i].y);
	}

	int M = P.size() - 1;

	if(M <= 2)return;

	int k = 0;

	while(k < M && trigArea(P[0],P[M - 1], P[k+1]) > trigArea(P[0],P[M - 1], P[k]) )k++;

	for (int i = 0; i < M; ++i) {
		while(trigArea( P[i],P[i+1], P[(k+1) % M] ) > trigArea(P[i],P[i+1], P[k]) ){
			k = ( (k + 1) % M);
		}

//		printf("%d %d k = %d\n",i,i+1,k);
//		printf("caliper for (%.2lf %.2lf) (%.2lf %.2lf) = (%.2lf %.2lf)\n",
//				P[i].x,P[i].y,P[i+1].x,P[i+1].y, P[k].x,P[k].y);
	}
}

int main(){

	P0.push_back(point(1,1));
	P0.push_back(point(3,3));
	P0.push_back(point(9,1));
	P0.push_back(point(12,4));
	P0.push_back(point(9,7));
	P0.push_back(point(1,7));
	P0.push_back(P0[0]);
	point pt(5,5);

	assert(fabs(perimeter(P0) - 31.638264) < 1e-5);
	assert(fabs(area(P0) - 49.000) < 1e-9);
	assert(isConvex(P0) == false);
	assert(inPolygon(pt,P0) == true);

//	printf("perimeter = %lf %lf\n", perimeter(P),perimeter(P) - 31.638264);
//	printf("area = %lf\n", area(P));
//	printf("isConvex = %d\n", isConvex(P));
//	printf("inPolygon = %d\n", inPolygon(pt,P));


	vector<point> nails;
	nails.push_back(point(0,0));
	nails.push_back(point(1,0));
	nails.push_back(point(2,0));
	nails.push_back(point(2,2));
	nails.push_back(point(0,2));

	vector<point> convexHull = CH(nails);
	vector<point> exp;
	exp.push_back(point(0,0));
	exp.push_back(point(2,0));
	exp.push_back(point(2,2));
	exp.push_back(point(0,2));
	exp.push_back(point(0,0));
//	printf("collinear = %d\n", collinear(nails[0],nails[1],nails[2]));

	assert(collinear(nails[0],nails[1],nails[2]) == true);

//	printf("convexHull:\n");
//	for (int i = 0; i < convexHull.size(); ++i) {
//		printf("%.2f %.2f\n",convexHull[i].x,convexHull[i].y);
//	}
//	printf("---\n");
	assert(convexHull == exp);

	RotatingCaliphers(P0);

	printf("All test passed\n");
	return 0;
}
