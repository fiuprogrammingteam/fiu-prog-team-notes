#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

//--------------------------------------------
// Vertex types
#define START_VERTEX 1
#define END_VERTEX 2
#define REGULAR_VERTEX 3
#define SPLIT_VERTEX 4
#define MERGE_VERTEX 5
#define x first
#define y second

/**
 * Lemma: Every triangulation of a polygon of n vertices uses n-3 diagonals and consist of
 * n-2 triangles
 */

class Vertex{
public:
	ii p;
	int type;
	Vertex *left, *right;
	Vertex (ii _p) : p(_p) , type(-1){
		left = right = NULL;
	}
};
class Edge{
	int helper=-1;
	Vertex *v1,*v2;
	Edge(Vertex *_v1,Vertex *_v2, int h) : v1(_v1),v2(_v2), helper(h){}
};


class vec{
public:
	int x,y;
	vec(){x = y = 0;}
	vec(int _x, int _y) : x(_x),y(_y){}
	vec(ii p1, ii p2) : x(p2.x-p1.x),y(p2.y-p1.y){}
};
vector<ii> Pn;
int n;


bool isBelow(ii p, ii q){
	if(p.y == q.y)
		return p.x > q.x;
	return p.y < q.y;
}

class cmp{
public:
	bool operator  () (Vertex v, Vertex w) const{
		return isBelow(v.p , w.p);
	}
};

/**
 * Left to right order.
 */
bool cmpEdge(Edge e1, Edge e2){
	// TODO
}

double cross(vec a, vec b){
	return a.x*b.y - a.y*b.x;
}

bool ccw(ii p, ii q, ii r){
	return cross(vec(p,q),vec(p,r)) > 0;
}

void data1();
void data2();
void data3();

int main(){
	FASTER;

	data3();

	Vertex * P = NULL, *F = NULL;
	// Create double-linked list and assign vertex types
	for (int i = 0; i < n; ++i) {

		int pr = (i-1+n) %n,nx = (i+1+n) %n;

		bool dir = ccw(Pn[pr], Pn[i],Pn[nx]);
		Vertex * v = new Vertex(Pn[i]);

		if(isBelow(Pn[pr],Pn[i]) && isBelow(Pn[nx], Pn[i]) && !dir)
			v->type = START_VERTEX;
		else if(isBelow(Pn[pr],Pn[i]) && isBelow(Pn[nx], Pn[i]) && dir)
			v->type = SPLIT_VERTEX;
		else if(!isBelow(Pn[pr],Pn[i]) && !isBelow(Pn[nx], Pn[i]) && !dir)
			v->type = END_VERTEX;
		else if(!isBelow(Pn[pr],Pn[i]) && !isBelow(Pn[nx], Pn[i]) && dir)
			v->type = MERGE_VERTEX;
		else
			v->type = REGULAR_VERTEX;

		if(P == NULL)
			F = P = v;
		else{
			v->left = P;
			P->right = v;
			P = v;
		}
	}

	// Connect last to first
	P->right = F;

	priority_queue<Vertex,vector<Vertex>,cmp> q;

//	for (int i = 0; i < n; ++i) {
//		q.push(*P);
//		printf("%d %d\n", P->p.x,P->p.y);
//		P = P->right;
//	}
//	printf("------------------\n");
//	while(!q.empty()){
//		printf("%d %d\n", q.top().p.x,q.top().p.y);
//		q.pop();
//	}

	//Complete this implementation
	printf("Not completed\n");

	return 0;
}

/**
 * Clock-wise polygon
 */
void data1(){
	Pn.clear();
	Pn.push_back(ii(0,0));
	Pn.push_back(ii(-2,2));
	Pn.push_back(ii(0,4));
	Pn.push_back(ii(4,2));
	Pn.push_back(ii(1,1));
	n = Pn.size();
}
/**
 * One split vertex
 */
void data2(){
	Pn.clear();
	Pn.push_back(ii(0,0));
	Pn.push_back(ii(-2,2));
	Pn.push_back(ii(0,4));
	Pn.push_back(ii(3,1));
	Pn.push_back(ii(1,2));
	n = Pn.size();
}
/**
 * One merge vertex
 */
void data3(){
	Pn.clear();
	Pn.push_back(ii(0,0));
	Pn.push_back(ii(-3,2));
	Pn.push_back(ii(-3,4));
	Pn.push_back(ii(-1,3));
	Pn.push_back(ii(0,4));
	Pn.push_back(ii(2,4));
	Pn.push_back(ii(3,3));
	n = Pn.size();
}
