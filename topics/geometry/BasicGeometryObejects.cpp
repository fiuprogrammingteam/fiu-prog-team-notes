#include <bits/stdc++.h>
using namespace std;

#define MEM(arr,val)memset((arr),(val), sizeof (arr))
#define PI (acos(0)*2.0)
#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

#define eps 1.0e-9
#define are_equal(a,b)fabs(a-b)<eps
#define LS(b)(b& (-b)) // Least significant bit
#define DEG_to_RAD(a)((a*PI)/180.0) // convert to radians
#define RAD_to_DEG(a)((a*180.0)/PI) // convert to radians

typedef long long ll;
typedef pair<int,int> ii;
typedef vector<int> vi;
typedef vector<ii> vii;

ll gcd(ll a,ll b){return b == 0 ? a : gcd(b,a%b);}
ll lcm(ll a,ll b){return a*(b/gcd(a,b));}

//----------------------------------------------------------------------//
struct point{
	double x,y;
	point(){x = y = 0.0;}
	point(double _x, double _y) : x(_x),y(_y){	}
	bool operator < (point other) const {
		if(!are_equal(x,other.x))
			return x < other.x;
		return y < other.y;
	}
	bool operator == (point other) const {
		return are_equal(x,other.x) && are_equal(y,other.y);
	}
};

struct vec{
	double x,y;
	vec(){x = y = 0.0;}
	vec(double _x, double _y) : x(_x),y(_y){}
	vec(point p1, point p2) : x(p2.x-p1.x),y(p2.y-p1.y){}
	vec scale(double s){
		return vec(this->x * s,this->y * s);
	}
};
/**
 * a*x + b*y + c = 0
 * a = -slope
 * c = -(y-intercept)
 */
struct line{
	double a,b,c;
	line(){a=b=c=0.0;}
	line(double _a,double _b,double _c): a(_a),b(_b),c(_c){}
	line(point p1, point p2){
		if(are_equal(p1.x,p2.x)){
			a = 1.0;
			b = 0.0;
			c = -p1.x;
		}else{
			a = -(double)(p1.y - p2.y)/(p1.x - p2.x);// slope
			b = 1.0;
			c = -(double)(a*p1.x)-p1.y;
		}
	}
};

// Lines and points
point translate(vec v, point p);
double dot(vec a, vec b);	// dot-product
double cross(vec a, vec b); // cross-product
double norm_sq(vec a);
double distToLine(point p, point a, point b, point &c);
double distToLineSegment(point p, point a, point b, point &c);

double dist(point p1, point p2);
point rotate(point p, double theta);
bool are_parallel(line l1, line l2);
bool are_same(line l1, line l2);
bool are_intersect(line l1, line l2, point &p);
vec toVec(point a, point b);

// returns angle aob in radians
double angle(point a, point o, point b);
bool ccw(point p, point q, point r);
bool collinear(point p, point q, point r);

// Circles
int insideCircle(point p, point c, double r){
	double dx = p.x - c.x, dy = p.y - c.y;
	return dx*dx + dy*dy < r*r;
}

// Triangles
double area(double ab, double bc, double ca);
double perimeter(double ab, double bc, double ca);
double rInCircle(point a, point b, point c);
double rInCircle(double ab, double bc, double ca);
int inCircle(point p1, point p2, point p3, point &ctr, double &r);


double rCircumCircle(point a, point b, point c);
double rCircumCircle(double ab, double bc, double ca);

void circumCircle(point p1, point p2, point p3, point &ctr, double &r);
bool circle2PtsRad(point p1, point p2, double r, point &c);

double gcDistance(double pLat, double pLong,
					double qLat, double qLong, double radius){
	pLat *= PI / 180;pLong *= PI / 180;
	qLat *= PI / 180;qLong *= PI / 180;

	return radius* acos(cos(pLat)*cos(pLong)*cos(qLat)*cos(qLong) +
			cos(pLat)*sin(pLong)*cos(qLat)*sin(qLong) +
			sin(pLat)*sin(qLat));
}

int main(){

	point p1(0,1),p2(0,1),p3(1,4),p4 = rotate(p3,90), p5(5,10);
	assert( p1 == p2);
	assert(!(p1 == p3));
//	printf("are equal %d\n", p1 == p2);
//	printf("are equal %d\n", p1 == p3);
//	printf("rotate((%.2f,%.2f)) = (%.2f,%.2f)\n", p3.x,p3.y,p4.x,p4.y);

	line l1(p1,p3),l2(p1,p4);
//	printf("line through ((%.2f,%.2f)) = (%.2f,%.2f) :  y = %.2fx + %.2f\n", p1.x,p1.y,p3.x,p3.y,-l1.a,-l1.c);
//	printf("line through ((%.2f,%.2f)) = (%.2f,%.2f) :  y = %.2fx + %.2f\n", p1.x,p1.y,p4.x,p4.y,-l1.a,-l1.c);
	point inter;
//	if(are_intersect(l1,l2,inter))
//		printf("(%.2f,%.2f)\n", inter.x,inter.y);
	assert( are_intersect(l1,l2,inter));
	point c;
	double d = distToLine(p4,p2,p3,c);
//	printf("min dist from (%.2f,%.2f) to line((%.2f,%.2f),(%.2f,%.2f))= %f\n", p4.x,p4.y,p2.x,p2.y,p3.x,p3.y, d);
//	printf("angle (%.2f,%.2f) (%.2f,%.2f) (%.2f,%.2f) = %.2f deg\n", p2.x,p2.y,p3.x,p3.y,p5.x,p5.y, RAD_to_DEG(angle(p2, p3,p5)));
//	printf("point (%.2f,%.2f) is on the %s side of the line ((%.2f,%.2f) (%.2f,%.2f))\n",
//			p2.x,p2.y,
//			ccw(p3,p5,p2) ? "left" : "right",
//			p3.x,p3.y,
//			p5.x,p5.y);



	// Triangle
	point t1(0,0),t2(10,0),t3(0,10), circle_center;
	double circle_rad;
	circumCircle(t1,t2,t3,circle_center,circle_rad);

//	printf("triangle (%.2f,%.2f)-(%.2f,%.2f)-(%.2f,%.2f)\n"
//			"perimeter = %.2f\n"
//			"area = %.2f\n"
//			"has a circum circle at (%.2f,%.2f) with radius = %.2f\n",
//			t1.x,t1.y,
//			t2.x,t2.y,
//			t3.x,t3.y,
//			perimeter(dist(t1,t2),dist(t2,t3),dist(t3,t1)),
//			area(dist(t1,t2),dist(t2,t3),dist(t3,t1)),
//			circle_center.x,circle_center.y,
//			circle_rad
//			);

//	printf("Area = %.3f r = %.3f\n", area(1.0,1.0,1.0),rCircumCircle(1.0,1.0,1.0));

	double r;
	circumCircle(point(0,0), point(0,1),point(1,0),c,r);
//	printf("c = (%.3f,%.3f) r = %.3f\n", c.x,c.y,r);

	printf("All test passed\n");
	return 0;
}


point translate(vec v, point p){return point(p.x + v.x, p.y + v.y);}
double dot(vec a, vec b){return a.x * b.x + a.y * b.y;}
double cross(vec a, vec b){return a.x*b.y - a.y*b.x;}
double norm_sq(vec a){return a.x * a.x + a.y * a.y;}

double distToLine(point p, point a, point b, point &c){
	vec ab(a,b),ap(a,p);
	double u = dot(ab,ap)/norm_sq(ab);
	c = translate(ab.scale(u), a);
	return dist(p,c);
}

double distToLineSegment(point p, point a, point b, point &c){
	vec ab(a,b),ap(a,p);
	double u = dot(ab,ap)/norm_sq(ab);
	if(u < 0.0){
		c = point(a.x,a.y);
		return dist(p,a);
	}
	if(u > 1.0){
		c = point(b.x,b.y);
		return dist(p,b);
	}
	return distToLine(p,a,b,c);
}

double dist(point p1, point p2){
	return hypot(p1.x - p2.x, p1.y- p2.y);
}
point rotate(point p, double theta){
	double rad = DEG_to_RAD(theta);
	return point(p.x*cos(rad) - p.y*sin(rad),
				p.x*sin(rad)+ p.y*cos(rad));
}

bool are_parallel(line l1, line l2){
	return are_equal(l1.a,l2.a) && are_equal(l1.b,l2.b);
}

bool are_same(line l1, line l2){
	return are_parallel(l1,l2) && are_equal(l1.c,l2.c);
}

bool are_intersect(line l1, line l2, point &p){
	if(are_parallel(l1, l2))return false;
	p.x = (l2.b * l1.c - l1.b * l2.c)/(l2.a * l1.b - l1.a * l2.b);

	if(fabs(l1.b) > eps)
		p.y = -(l1.a * p.x + l1.c);
	else
		p.y = -(l2.a * p.x + l2.c);
	return true;
}

vec toVec(point a, point b){
	return vec(b.x - a.x, b.y - a.y);
}

// oa*ob = |oa|*|ob|*cos(theta), theta = arccos(oa*ob / (|oa|*|ob|) )
double angle(point a, point o, point b){
	vec oa(o,a), ob(o,b);
	return acos(dot(oa,ob)/ sqrt(norm_sq(oa)*norm_sq(ob)));
}

// returns true if point r is on the left side of the line pq
bool ccw(point p, point q, point r){
	return cross(vec(p,q),vec(p,r)) > 0;
}

// returns true if point r is on the same line as the line pq
bool collinear(point p, point q, point r){
	return fabs(cross(vec(p,q),vec(p,r)) )<  eps;
}

// Heron's formula
double area(double ab, double bc, double ca){
	double s = perimeter(ab,bc,ca)/2.0;
	return sqrt(s*(s-ab)*(s-bc)*(s-ca));
}

double perimeter(double ab, double bc, double ca){
	return ab + bc + ca;
}

double rInCircle(point a, point b, point c){
	return rInCircle(dist(a,b),dist(b,c),dist(c,a));
}

double rInCircle(double ab, double bc, double ca){
	double s = perimeter(ab,bc,ca)/2.0;
	return area(ab,bc,ca)/s;
}

int inCircle(point p1, point p2, point p3, point &ctr, double &r){
	r = rInCircle(p1,p2,p3);
	if(fabs(r) < eps)return 0;

	double ratio = dist(p1,p2) / dist(p1,p3);
	point p = translate(vec(p2,p3).scale(ratio/(1 + ratio)), p2);

	line l1(p1,p);

	ratio = dist(p2,p1) / dist(p2,p3);
	p = translate(vec(p1,p3).scale(ratio/(1 + ratio)), p1);
	line l2(p2,p);
	are_intersect(l1,l2,ctr);
	return 1;
}


double rCircumCircle(double ab, double bc, double ca){
	return ab * bc * ca / (4.0*area(ab,bc,ca));
}
double rCircumCircle(point a, point b, point c){
	return rCircumCircle(dist(a,b),dist(b,c),dist(c,a));
}

void circumCircle(point p1, point p2, point p3, point &ctr, double &r){

	r = rCircumCircle(p1,p2,p3);

	// first bisector
	vec ab(p1,p2);
	vec i_ab(-ab.y,ab.x);	// perpendicular vector
	ab = ab.scale(0.5);

	point b1 = translate(ab,p1);
	point b2 = translate(i_ab,b1);
	line l1(b1,b2);
	// second bisector
	vec bc(p2,p3);
	vec i_bc(-bc.y,bc.x); // perpendicular vector
	bc = bc.scale(0.5);

	point b3 = translate(bc,p2);
	point b4 = translate(i_bc,b3);
	line l2(b3,b4);
	are_intersect(l1,l2,ctr);

}

bool circle2PtsRad(point p1, point p2, double r, point &c){

	double d2 = hypot(p1.x - p2.x, p1.y - p2.y) ;

	double det = r*r / d2 - 0.25;
	if(det < 0.0)return false;

	double h = sqrt(det);
	c.x = (p1.x + p2.x)*0.5 + (p1.y - p2.y)*h;
	c.y = (p1.y + p2.y)*0.5 + (p2.x - p1.x)*h;

	return true;
}
