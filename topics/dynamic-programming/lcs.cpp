/*
 * lcs.cpp
 *
 *  Created on: Nov 21, 2014
 *      Author: giuseppe
 */
#include <cstdio>
#include <string.h>

#define max(a,b)(a>b?a:b)

int main(){

//	int seq1[] = {1,2,5,10,12,20,21,22,6,7};
//	int seq2[] = {1,2,5,10,12,19,21,22,6,8};

	int seq1[] = {1,5,3};
	int seq2[] = {4,5,6,7};

	int l1 = sizeof(seq1)/sizeof(int);
	int l2 = sizeof(seq2)/sizeof(int);

//	printf("%d %d\n", l1,l2);

	int dp[l1+1][l2+1];
	memset(dp, 0, sizeof(dp));

	for(int i = 0 ; i <= l1 ; i++)dp[i][0] = 0;
	for(int i = 0 ; i <= l2 ; i++)dp[0][i] = 0;


	for(int i = 1 ; i <= l1 ; i++){
		for(int j = 1 ; j <= l2 ; j++){
			if(seq1[i-1] == seq2[j-1]){
				dp[i][j] = dp[i-1][j-1] + 1;
			}else{
				dp[i][j] = max(dp[i-1][j],dp[i][j-1]);
			}
		}
	}

	// Print lcs
	int i = l1, j = l2;
	while(!(i == 0 || j == 0)){

		if(seq1[i-1] == seq2[j-1]){
			printf("%d ", seq1[i-1]);
			i--,j--;
		}else{
			if(dp[i-1][j] > dp[i][j-1])
				i--;
			else
				j--;
		}
	}

	printf("\nlcs = %d\n", dp[l1][l2]);

}
