/*
    What is the most profitable way to cut a n wide stick
    given the price of each witdh
*/
#include <cstdio>
#define max(a,b)a>b?a:b

// int MEM[];

// int dp(int *prices, int n){
//     if(n == 0)return 0;
    
//     int best = -10000000;
//     for(int i = 1 ; i <= n ; i++){
//         best = max(best, dp(prices, n - i)+ prices[i]);
//     }
//     return best;
// }

int main(){
    FILE *f;
    f = fopen("cutting.in", "r");
    int T, n, price[100];
    
    fscanf(f,"%d", &T);
    while(T--){
        
        fscanf(f,"%d", &n);
        for(int i = 1 ; i <= n ; i++){
            fscanf(f,"%d", price + i);
        }
        
        int dp[n + 1];
        dp[0] = 0;
        
        for(int i = 1; i <= n ; i++){
            dp[i] = -10000;
            for(int j = 1; j <= i ; j++){
                dp[i] = max(dp[i], price[j] + dp[i - j]);
            }
        }
        
        printf("max profits: %d\n", dp[n]);
        // printf("max profits: %d\n", dp(prices, n));
        
    }
    return 0;
}
