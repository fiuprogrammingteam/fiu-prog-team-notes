# FIU Programming Team Notes

## Purpose

This repo servers the purpose of centralizing our team's knowledge base of algorithms. The code here should be well tested and documented.

Currently, code will be written in C++ to ensure a standard language for all memebers to facilitate communication and collaboration both in and out of competitive settings. There are currently no plans to include other languages.

## Contributing

To help contribute to the code base of this repository, users should clone the reposiory to their own machines and develop there according to [predefined guidelines](docs/guidelines.md). Once the changes have been committed on a seperate branch, a pull request should be submitted with [guisevtr](https://github.com/giusevtr) as the reviewer.
