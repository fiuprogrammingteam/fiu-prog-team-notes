
#include <bits/stdc++.h>
using namespace std;

#define forn(i,n) for(int (i) = 0; (i) < (n); (i)++)
#define fornm(i,n,m) for(int (i) = (n); (i) < (m); (i)++)
#define dprintf if(debugging)printf

#define FASTER ios_base::sync_with_stdio(false); cin.tie(NULL); cout.tie(NULL)

const int N = 30002, Q = 20002, A = 1000002;
int sqrtN = sqrt(N); // Size of each range-bucket

typedef struct query {
	int l, r, idx;
} Query;

int n, q, ans, currL = 0, currR = 0;

int nums[N], counter[A], answers[Q];
Query queries[Q];

bool compQueries(query q1, query q2) {
	if (q1.l/sqrtN != q2.l/sqrtN)
		return q1.l < q2.l;
	return q1.r < q2.r;
}

void add(int idx) {
	counter[nums[idx]]++;
	if (counter[nums[idx]] == 1)
		ans++;
}

void remove(int idx) {
	counter[nums[idx]]--;
	if (counter[nums[idx]] == 0)
		ans--;
}

int main() {
	FASTER;
	
	scanf("%d", &n);
	forn(i,n) // Read in nums
		scanf("%d", &nums[i]);
	
	scanf("%d", &q);
	forn(i,q) { // Read in queries
		queries[i].idx = i;
		scanf("%d %d", &queries[i].l, &queries[i].r);
		queries[i].l--; queries[i].r--;
	}
	
	sort(queries, queries+q, compQueries);
	
	forn(i,q) {
		Query query = queries[i];
		
		while(currL < query.l) {// Shift currL right
			remove(currL);
			currL++;
		}
		
		while(currL > query.l) {// Shift currL left
			add(currL - 1);
			currL--;
		}
		
		while(currR < query.r + 1) { // Shift currR right
			add(currR);
			currR++;
		}
		
		while(currR > query.r + 1) { // Shift currL left
			remove(--currR);
		
		answers[query.idx] = ans;
	}
	
	forn(i,q)
		printf("%d\n", answers[i]);
	
	return 0;
}

