# Weekly Topics

## Purpose

To facilitate the disemination of new information amongst team members, every week, each team member is to pick a topic from our [master list of topics](topic-list.md) and reserve it for a specific week. At the end of each week, the member will present their topic to the other members with some sort of implementation. The goal is to have our covered topics centralized and organized. This is so that new members could look through our history and catch up independently and current members could more easily maintain a consistent, rapid pace of learning.

## Format

Each new week, a new weekly folder will be created in the `weeks` folder with name format `YYYYMMDD` (i.e. the week of Sun July 17th, 2016 will be called `20160717`). In this folder will be a `README.md` that should contain a list of the topics that will be covered that week along with the member that will be covering that topic that week. Within that week's folder, there should also be a folder for each topic that will be covered. Optionally, there will also be a link to a brief description of the topic within that week's list.

## Member Responsibilities

At the begining of each week, each member is to do the following:

 1. Pick a topic from the [master list of topics](topic-list.md)
 2. Add the chosen topic to that week's topic list
 3. Add their name next to the topic in that week's topic list
 4. Remove their chosen topic from the [master list of topics](topic-list.md)
 5. Create a folder with their topic's name within that week's folder
 6. Add any relevant source code, presentations, documentations, etc. to the folder from step 5

