#!/bin/bash
FILES=../topics/*
for f in $FILES
do
  if [ -d "$f" ]; then
    echo "----$f------"
    cd "$f"
    for p in *.cpp
    do
      echo "  Testing: $p"
      g++ -std=c++11 "$p"
      ./a.out
    done
    cd ..
   # echo "All $f test passed"

  #elif [ -f "$f" ];then
    #echo "$f is a file"
  fi
done
